﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace UebungPA
{
    class Kreis : Figur
    {
        private Punkt mitte;
        public Punkt Mitte
        {
            get { return mitte.Clone(); }
            set { 
                mitte = value.Clone();
                _radius = null;
            }
        }

        private Punkt rand;
        public Punkt Rand
        {
            get { return rand.Clone(); }
            set { 
                rand = value.Clone();
                _radius = null;
            }
        }
        
        private int? _radius;
        public int Radius
        {
            get {
                if (_radius == null)
                {
                    _radius = dist(Mitte, Rand);  
                }
                return (int)_radius;
            }
        }
        
        public Kreis(Punkt MittelPunkt, Punkt RandPunkt)
        {
            this.Mitte = MittelPunkt;
            this.Rand = RandPunkt;
        }

        private int dist(Punkt a, Punkt b)
        {
            int dx = a.X - b.X;
            int dy = a.Y - b.Y;

            if (dx == 0 && dy == 0) return 0;

            return (int)Math.Round(Math.Sqrt(dx * dx + dy * dy), 0);
        }

        private Rectangle getRectangle()
        {
            return new Rectangle(mitte.X - Radius, mitte.Y - Radius, 2 * Radius, 2 * Radius);
        }

        public void Paint(Graphics g, bool points = false, Pen penCircle = null, Pen penPoints = null)
        {
            var penC = penCircle ?? Pens.DarkRed;
            g.DrawEllipse(penC, getRectangle());
            if (points)
            {
                mitte.Paint(g, true, penPoints, penPoints);
                rand.Paint(g, true, penPoints, penPoints);
            }
        }

        public bool Contains(int x, int y)
        {
            if (mitte.Contains(x, y) || rand.Contains(x, y)) return true;

            /* da funktion für rechteck da ist, 
             * können wir überprüfen ob sich die koordinate im umgebenden 4-Eck ist.
             * Die kosten dafür ist das anlegen eines Rectangle structs, ansonsten 
             * müssten wir immer Quadrieren, Wurzel ziehen und Runden - was mehr aufwand ist. */
            var rect = getRectangle();
            if (!rect.Contains(x, y)) return false;

            int d = dist(mitte, new Punkt(x, y));
            return d <= Radius;
        }


        public int PointClicked(int x, int y)
        {
            if (mitte.Contains(x, y))     return 0;
            else if (rand.Contains(x, y)) return 1;
            else                          return -1;
        }
    }
}
