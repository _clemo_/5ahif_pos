﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace UebungPA
{
    interface Figur // fasst punkt, kreis und Rechteck zusammen
    {
        bool Contains(int x, int y);
        int PointClicked(int x, int y);
        void Paint(Graphics g, bool points = false, Pen a = null, Pen b = null);
    }
}
