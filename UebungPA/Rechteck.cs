﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace UebungPA
{
    class Rechteck : Figur
    {
        private Punkt a;
        public Punkt A
        {
            get { return a.Clone(); }
            set { a = value.Clone(); }
        }

        private Punkt b;
        public Punkt B
        {
            get { return b.Clone(); }
            set { b = value.Clone(); }
        }

        public Rechteck(Punkt a, Punkt b)
        {
            A = a;
            B = b;
        }

        private Rectangle getRect()
        {
            int x = a.X;
            int y = a.Y;
            int w = b.X-x;
            int h = b.Y-y;

            if (w < 0)
            {
                x += w;
                w = -w;
            }
            if (h < 0)
            {
                y += h;
                h = -h;
            }
            return new Rectangle(x, y, w, h);
        }

        public void Paint(Graphics g, bool points = false, Pen penCircle = null, Pen penPoints = null)
        {
            var pen = penCircle ?? Pens.DarkGreen;
            g.DrawRectangle(pen, getRect());
            if (points)
            {
                a.Paint(g, true, penPoints, penPoints);
                b.Paint(g, true, penPoints, penPoints);
            }
        }

        public bool Contains(int x, int y)
        {
            if (a.Contains(x, y) || b.Contains(x, y)) return true;
            return getRect().Contains(x, y);
        }


        public int PointClicked(int x, int y)
        {
            if (a.Contains(x, y))      return 0;
            else if (b.Contains(x, y)) return 1;
            else                       return -1;
        }
    }
}
