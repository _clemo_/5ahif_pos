﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UebungPA
{
    public partial class Form1 : Form
    {
        List<Figur> l = new List<Figur>(){null};
        bool isKreis = false;
        int movePoint = 1;
        bool points = false;

        int thinkness = 2;
        Color ColorKreis = Color.DarkRed;
        Color ColorRechteck = Color.DarkBlue;
        Color ColorPoint = Color.DarkOrange;
        
        public Form1()
        {
            InitializeComponent();

            // drucken:
            pd.BeginPrint += pd_BeginPrint;
            pd.PrintPage += pd_PrintPage;
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            radioButton1.Enabled = radioButton2.Enabled = false;

            for(int i = 0, max = l.Count; i < max; ++i) // stelle fest ob auf ´punkt geklickt wurde
            {
                var item = l[i];
                if (item == null) continue;
                int c = item.PointClicked(e.X, e.Y); // c: -1 > nicht geklickt, 0 > erster punkt, 1 > 2ter punkt
                if (c != -1)
                {
                    movePoint = c; // movepoint gibt an welcher punkt verschoben wird

                    // wenn letztes element null > es wird nichts geschoben, daher null element löschen 
                    // es wird immer das letzte element verschoben > löschen und am ende wieder einfügen
                    l.Remove(item); 
                    l.Remove(null);
                    l.Add(item);

                    // stelle fest ob es ein krei is
                    if (item is Kreis)
                        isKreis = true;
                    Invalidate();
                    return; // springe aus funktion
                }
            }

            movePoint = 1; // verschiebe 2ten punkt (erster = wo angefangen wurde zu klicken)
            isKreis = radioButton1.Checked;
            if (isKreis)
            {
                l[l.Count - 1] = new Kreis(new Punkt(e.X, e.Y), new Punkt(e.X, e.Y));
            }
            else
            {
                l[l.Count - 1] = new Rechteck(new Punkt(e.X, e.Y), new Punkt(e.X, e.Y));
            }
            Invalidate();
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (l.Last() == null)
            { // f == null > es wird gerade nciht gezeichnet      
                foreach (var item in l) // prüfe ob maus über einem element ist
                {
                    if(item != null)
                        if (item.Contains(e.X, e.Y)) // contains schließt auch die punkte mit ein
                        {
                            if (!points) // damit nicht unnötig oft neu gezeichnet wird
                            {
                                points = true; // zeige punkte
                                Invalidate();
                            }
                            return;
                        }
                }
                if (points) // damit nicht unnötig oft neu gezeuchnet wird
                {
                    points = false; // zeige keine punkte
                    Invalidate();
                }
            }else{  // zeichne
                var p = new Punkt(e.X, e.Y); // lege punkt für aktuelle position an
                if (isKreis)
                {
                    var k = l.Last() as Kreis;
                    if (movePoint == 1) // ändere punkt der verschoben werden soll
                        k.Rand = p;
                    else
                        k.Mitte = p;
                }
                else
                {
                    var r = l.Last() as Rechteck;
                    if (movePoint == 1)  // ändere punkt der verschoben werden soll
                        r.B = p;
                    else
                        r.A = p;
                }
                Invalidate();
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            radioButton1.Enabled = radioButton2.Enabled = true;
            l.Add(null);
            Invalidate();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            if (l.Count > 0) // damit es nciht verpixelt aussieht
            {
                e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            }

            bool points = this.points;
            if (!this.points)
                points = l.Last() != null;
            Pen pk = new Pen(ColorKreis,thinkness);
            Pen pR = new Pen(ColorRechteck, thinkness);
            Pen pP = new Pen(ColorPoint, thinkness);

            foreach (var item in l)
            {
                if (item != null)
                    item.Paint(e.Graphics, points, (item is Kreis?pk:pR),pP);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var fa = new FarbAuswahl(ColorPoint, ColorRechteck, ColorKreis, thinkness);
            fa.PointColorChanged += fa_PointColorChanged;
            fa.RectColorChanged += fa_RectColorChanged;
            fa.CircleColorChanged += fa_CircleColorChanged;
            fa.ThiknessChanged += fa_ThiknessChanged;
            fa.Show();
        }

        void fa_ThiknessChanged(int t)
        {
            thinkness = t;
            Invalidate();
        }

        void fa_CircleColorChanged(Color c)
        {
            ColorKreis = c;
            Invalidate();
        }

        void fa_RectColorChanged(Color c)
        {
            ColorRechteck = c;
            Invalidate();
        }

        void fa_PointColorChanged(Color c)
        {
            ColorPoint = c;
            Invalidate();
        }

        /*** DRUCKEN ***/
        PrintDocument pd = new PrintDocument();
        List<string> printQueue = new List<string>();
        private int header = 0, footer = 0;
        private Margins margins = null;
        private static Font arial = new Font("Arial", 12f);
        private int pagecount;
        private float? lineHeight = null;

        int readyToPrint = 0;

        private string charset = "abdAdSAFNASDNMaldG";
        private void button2_Click(object sender, EventArgs e)
        {
            // Datei auswählen
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Druckdatei wählen";
            var res = ofd.ShowDialog();
            if (res != System.Windows.Forms.DialogResult.OK) return;

            // Datei einlesen
            StringBuilder sb = new StringBuilder();
            using (var sr = new StreamReader(ofd.FileName))
            {
                sb.Append(sr.ReadToEnd());
            }
            sb.Replace("\r\n", "\n").Replace("\n\r", "\n");
            printQueue.Clear();
            printQueue = new List<string>(sb.ToString().Split('\n'));

            // optional: charset ermitteln
            var charl = new List<char>();
            var _chars = from Item in printQueue select Item.ToCharArray().Distinct();
            foreach (var item in _chars)
                charl.AddRange(item);
            charset = String.Concat(charl.Distinct().ToArray());

            // druckeinstellungen
            Func<int, int> toMM =
                (inch) =>
                {
                    return (int)Math.Round((inch * 0.254f), 0);
                };

            Func<int, int> toIN =
                (mm) =>
                {
                    return (int)Math.Round((mm * (1 / 0.254f)), 0);
                };

            margins = pd.DefaultPageSettings.Margins;
            margins = new Margins(toMM(margins.Left), toMM(margins.Right), toMM(margins.Top), toMM(margins.Bottom));
            header = toMM(header);
            footer = toMM(footer);
            PrintSettings.GetSettings(ref margins, ref header, ref footer);
            pd.DefaultPageSettings.Margins = new Margins(toIN(margins.Left), toIN(margins.Right), toIN(margins.Top), toIN(margins.Bottom));
            header = toIN(header);
            footer = toIN(footer);

            // druckvorschau
            PrintPreviewDialog ppd = new PrintPreviewDialog();
            ppd.Document = pd;
            ppd.ShowDialog();
        }

        List<string> splitLine(string line, Graphics g, int width, Font f)
        {
            List<string> Line = new List<string>() { line}; // alle teile in die er die zeile zerlegt hat
            char[] seperator = new char[] { '.', ',', '-', '!', '?', ' ' }; // alle trennzeichen mit denen er die seiten splittet
            string prefix = "...";
            int part = 0;
            do
            {
                string l = Line[part]; // der aktuelle teil der zeile der behandelt wird
                int len = l.Length;    // länge des zu benutzenden teiles
                string t = "";
                while (g.MeasureString(t = l.Substring(0, len), f).Width > width)
                { // teste wie breit der string ist, wenn zu breit
                    var tmp = t.LastIndexOfAny(seperator); // finde den letzten punkt an dem gesplittet werden kann
                    if (tmp == -1) // wenn kein punkt, dann muss die länge passen
                        break;
                    else
                        len = tmp; // neue länge des teilstrings
                }

                Line[part++] = t; // schreibe in liste neues element und erhöhe zähler
                if (l.Length != t.Length) // wenn ein teilstring über bleibt diesen als neuen teil einfügen
                    Line.Insert(part, prefix + l.Substring(len));
            } while (part < Line.Count); // wiederhole bis alle teile der linie untersucht sind
            return Line;
        }

        void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            // calculation
            if(lineHeight == null)
                lineHeight = e.Graphics.MeasureString(charset, arial).Height;
            float lh = (float)lineHeight;
            var lineSpaceing = 2;
            var p = new Margins(
                e.PageBounds.Left + margins.Left,
                e.PageBounds.Width -(margins.Right+margins.Left+e.PageBounds.Left),
                e.PageBounds.Top + margins.Top,
                e.PageBounds.Height - (margins.Bottom+margins.Top+e.PageBounds.Top));
            var height = p.Bottom - p.Top;
            var width = p.Right-p.Left;
            var black = new SolidBrush(Color.Black);

            // kopf und fußzeile
            e.Graphics.FillRectangle(
                new SolidBrush(Color.Yellow),
                new Rectangle(p.Left, p.Top, width, header));
            e.Graphics.FillRectangle(
                new SolidBrush(Color.Yellow),
                new Rectangle(p.Left, p.Bottom - footer, width, footer));

            ++pagecount;
            e.Graphics.DrawString(pagecount.ToString(), arial, black, new PointF(p.Left, p.Bottom - lh));

            // rand
            e.Graphics.DrawRectangle(Pens.Red, new Rectangle(p.Left, p.Top, width, height));

            // adapt page settings
            p.Top += header;
            p.Bottom -= footer;
            height -= (header + footer);

            // drucken von text
            int i = p.Top;
            do
            {
                string l = printQueue.First();
                printQueue.RemoveAt(0);
                if (readyToPrint-- <= 0) // damit nicht passende zeilen mehrfach überprüft werden
                {
                    List<string> parts = splitLine(l, e.Graphics, width, arial); // zerlege zeile in passende teile

                    for (int x = 1; x < parts.Count; ++x) // füge teile in die queue ein
                        printQueue.Insert(x - 1, parts[x]);

                    l = parts[0]; // wähle aktuellen teil
                    readyToPrint = parts.Count - 1; // setzte count mit korrekten zeilen
                }
                e.Graphics.DrawString(l, arial, black, new PointF(p.Left,i)); // schreibe zeile
                i += (int)Math.Round(lh+ lineSpaceing,0); // errechne begin nächste zeile
            } while (i+lh < p.Bottom && printQueue.Count > 0); // prüfe ob nächste zeile noch platz hat oder fertig ist

            e.HasMorePages = printQueue.Count > 0; // wenn noch nicht fertig > nächste seite
        }

        void pd_BeginPrint(object sender, PrintEventArgs e)
        {
            pagecount = 0;
            lineHeight = null;
        }
    }
}
