﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UebungPA
{
    public partial class PrintSettings : Form
    {
        private int hh;
        private int ht;
        private int t;
        private int b;
        private int l;
        private int r;
        
        private PrintSettings(Margins PageMargins, int heightHeader, int HeightTrailer)
        {
            InitializeComponent();
            hh = heightHeader;
            ht = HeightTrailer;
            t  = PageMargins.Top;
            b  = PageMargins.Bottom;
            l  = PageMargins.Left;
            r  = PageMargins.Right;

            txtBottom.Text  = "" + b;
            txtHeader.Text  = "" + hh;
            txtLeft.Text    = "" + l;
            txtRight.Text   = "" + r;
            txtTrailer.Text = "" + ht;
            txtTop.Text     = "" + t;
        }

        public static void GetSettings(ref Margins PageMargins, ref int heightHeader, ref int HeightTrailer)
        {
            var ps = new PrintSettings(PageMargins, heightHeader, HeightTrailer);
            ps.ShowDialog();
            PageMargins.Bottom = ps.b;
            PageMargins.Top = ps.t;
            PageMargins.Left = ps.l;
            PageMargins.Right = ps.r;
            heightHeader = ps.hh;
            HeightTrailer = ps.ht;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            hh = Int32.Parse(txtHeader.Text);
            b  = Int32.Parse(txtBottom.Text);
            l  = Int32.Parse(txtLeft.Text);
            r  = Int32.Parse(txtRight.Text);
            ht = Int32.Parse(txtTrailer.Text);
            t  = Int32.Parse(txtTop.Text);
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
