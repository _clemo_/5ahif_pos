﻿// Um schneller arbeiten zu können sollte man unbedingt Snippets und var nutzen. 
// var ist ein datentyp, beid em der compiler wählt welcher hingehört.
// var kann nur benutzt werden wenn der compiler den datentyp bestimmen kann.
// das ist möglich wenn der datentyp rechts vorkommt. bsp: var a = (int)o; oder var b = new StringBuilder();
// oder es eindeutig ist durch iteration (siehe foreach, dort ist item ein int, da int[] durchgeangen wird)
// Einfach Snippetnamen und 2 mal Tabulator drücken.
// bei snipptes gibt es auch variabeln (wie myvar2), wird ein vorkommen gändert, 
// ändern sich die anderen innerhalb des snippets ebenso
// Die wichtigsten:


class Test
{
    //propg: erzegt ein property dass vona ußen nur gelesen werden kann
    public int MyProperty { get; private set; }


    // propfull
    private int myVar2;

    public int MyProperty2
    {
        get { return myVar2; }
        set { myVar2 = value; }
    }
    

    //ctor: erzeugt einen konstruktor
    public Test()
    {
        int[] collection = new int[] { };

        //foreach
        foreach (var item in collection)
        {
            
        }
    }
}