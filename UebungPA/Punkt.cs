﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace UebungPA
{
    class Punkt : Figur
    {
        public static int Size = 10;

        private int x;
        private int y;
        private Rectangle? r; // das ? gibt an, dass auch null-werte erlaubt sind. bsp: int?

        public int X {
            get
            {
                return x;
            }

            private set
            {
                if (value != x)
                {
                    r = null;
                    x = value;
                }
            }
        }
        public int Y {
            get
            {
                return y;
            }

            private set
            {
                if (value != y)
                {
                    r = null;
                    y = value;
                }
            }
        }
        
        public Punkt(int x, int y)
        {
            X = x;
            Y = y;
        }

        private Rectangle GetRect()
        {
            if (r == null) // lazy-Load: prüfe ob ojket schon vorhanden, dann erzeuge es, sonst nehme bestehndes
            {
                int hs = Size / 2;
                r=new Rectangle(X - hs, Y - hs, Size, Size);
            }

            return (Rectangle)r;
        }

        public void Paint(Graphics g, bool points = false, Pen p = null, Pen inner = null) // p ist optional
        {
            if (points)
            {
                var Pen = p ?? Pens.Blue; // wenn p == null > Blau
                var rect = GetRect();
                g.DrawRectangle(Pen, rect);

                if (inner != null) 
                { // nur um das Interface Figur zu implementieren: macht einen kleinen Punkt in der mitte
                    var _p = new Point(x-1,y-1);
                    g.FillRectangle(new SolidBrush(inner.Color), new Rectangle(_p, new Size(2, 2)));
                }
            }
        }

        public bool Contains(int x, int y)
        {
            return GetRect().Contains(x, y);
        }

        public Punkt Clone()
        {
            return new Punkt(x,y);
        }

        public override string ToString()
        {
            return "{"+X+", "+Y+"}";
        }


        public int PointClicked(int x, int y)
        {
            if (Contains(x, y)) return 0;
            else                return -1;
        }
    }
}
