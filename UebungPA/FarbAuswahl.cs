﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UebungPA
{
    public delegate void ColorChangedHandler(Color c);
    public delegate void ThiknessChangedHandler(int t);
    public partial class FarbAuswahl : Form
    {
        public event ColorChangedHandler PointColorChanged;
        public event ColorChangedHandler RectColorChanged;
        public event ColorChangedHandler CircleColorChanged;
        public event ThiknessChangedHandler ThiknessChanged;

        private Color[] cols;

        public FarbAuswahl(Color cp, Color cr, Color cc, int t)
        {
            cols = new Color[] { cp, cr, cc };
            InitializeComponent();
            trackBar1.Maximum = 10;
            trackBar1.Minimum = 1;
            trackBar1.Value = t;
        }

        private void set()
        {
            cdPoint.Color = cols[0];
            cdRect.Color = cols[1];
            cdCirc.Color = cols[2];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            set();
            var res = cdPoint.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                cols[0] = cdPoint.Color;
                if (PointColorChanged != null)
                    PointColorChanged(cols[0]);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            set();
            var res = cdRect.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                cols[1] = cdRect.Color;
                if (RectColorChanged != null)
                    RectColorChanged(cols[1]);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            set();
            var res = cdCirc.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                cols[2] = cdCirc.Color;
                if (CircleColorChanged != null)
                    CircleColorChanged(cols[2]);
            }
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            if (ThiknessChanged != null)
                ThiknessChanged(trackBar1.Value);
        }
    }
}
