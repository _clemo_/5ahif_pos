// CPP_SWAP.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"

#include <iostream>

using namespace std;

void swap(int *a, int *b){
    int t = *b; // value of b
    *b = *a;    // value b = value a
    *a = t;     // value a = t
}


int main(){
	int a = 10, b = 20;
	cout << a << " " << b << endl;
	swap(&a, &b);
	cout << a << " " << b << endl;
	cin>>a; // wait for input
	return 0;
}

/*int _tmain(int argc, _TCHAR* argv[])
{
	return 0;
}*/

