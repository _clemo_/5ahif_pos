// CPP_Tree.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include <iostream>
#include "Tree.h"

using namespace std;

void add_rec(TreeItem** t, int val){
    TreeItem* c = *t;                   // current element

    if(c == 0){                         // element == NULL
        TreeItem* elem = new TreeItem;  // erstelle neues element
        elem->value = val;              // elem = speicheradresse neues element
        elem->left = 0;                 // t = speicheradresse der speicheradresse von Element
        elem->right = 0;                // *t = speicheradresse von element

        (*t) = elem;                    // �berschreibe speicheradresse von element
    }else{
        int cval = c->value;            // wert des aktuellen knotens
        if(cval == val)                 // wenn wert schon in baum > fertig
            return;
        else if(cval > val)             // aktueller knoten > zielwert
            add_rec(&(c->left), val);   // zielknoten muss links von aktuellem knoten eingef�gt werden
        else
            add_rec(&(c->right), val);  // ansonsten rechts von aktuellem knoten einf�gen
    }
}

TreeItem* add(TreeItem* t, int val){
    TreeItem* elem = new TreeItem;
    elem->value = val;
    elem->left = 0;
    elem->right = 0;

    if(t == 0)
        return elem;

    TreeItem* ptr = 0;
    TreeItem* next = t;
    bool goLeft = true;
    while(next != 0){
        ptr=next;
        if(ptr->value == val)
            return t;
        if(goLeft = (ptr->value > val)){
            next = ptr->left;
        }else{
            next = ptr->right;
        }
    }

    if(goLeft)
        ptr->left = elem;
    else
        ptr->right = elem;

    return t;
}

void print(TreeItem* t){
    if(t == 0) return;
    
    if(t->left != 0){
        print(t->left);
        cout << " ";
    }
    
    cout << t->value;

    if(t->right != 0){
        cout << " ";
        print(t->right);
    }
}

int main(){
    TreeItem* tree1 = 0;
    add_rec(&tree1, 5);
    add_rec(&tree1, 3);
    add_rec(&tree1, 8);
    add_rec(&tree1, 4);
    add_rec(&tree1, 6);
    add_rec(&tree1, 8);

    print(tree1);

    int x;
    cin >> x;
    return 0;
}

/*
int _tmain(int argc, _TCHAR* argv[])
{
	return 0;
}*/

