#pragma once

struct TreeItem{
    int value;
    TreeItem* left;
    TreeItem* right;
};

TreeItem* add(TreeItem* t, int val);
void print(TreeItem* t);