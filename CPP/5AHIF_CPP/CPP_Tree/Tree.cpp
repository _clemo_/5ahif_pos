#include "Tree.h"
#include "stdafx.h"
#include <iostream>

using namespace std;

TreeItem* add(TreeItem* t, int val){
    TreeItem* elem = new TreeItem;
    elem->value = val;
    elem->left = 0;
    elem->right = 0;

    if(t == 0)
        return elem;

    TreeItem* ptr = 0;
    TreeItem* next = t;
    bool goLeft = true;
    while(next != 0){
        ptr=next;
        if(goLeft = (ptr->value < val)){
            next = ptr->left;
        }else{
            next = ptr->right;
        }
    }

    if(goLeft)
        ptr->left = elem;
    else
        ptr->right = elem;

    return t;
}

void print(TreeItem* t){
    if(t == 0) return;
    
    if(t->left != 0)
        print(t->left);
    
    cout << t->value;

    if(t->right != 0)
        print(t->right);
}