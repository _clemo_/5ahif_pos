// CPP_Heap.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//
#include "heapsort.h" // funkionen des heapsorts
#include "Tests.h"    // testfunktionen
#include <iostream>

void testPrepA(void);
void testPrepB(void);
void testHeapifyA(void);
void testHeapifyB(void);
void testSortA(void);
void testSortB(void);

int main(){
    testPrepA();
    testPrepB();
    testHeapifyA();
    testHeapifyB();
    testSortA();
    testSortB();
    return 0; 

    // benutzung:
        int testdata[] = {5, 4, 8, 2, 9, 1, 7};
        int size = 7;
        heapsort(testdata, size);
        print(testdata, size);

    return 0; 
}

/*
int _tmain(int argc, _TCHAR* argv[])
{
	return 0;
}*/

void testPrepA(void){
    int testdata[] = {5, 4, 8, 2, 9, 1};
    prepareHeap(testdata, 6);

    bool correct =
        testdata[0] == 9 &&
        testdata[1] == 5 &&
        testdata[2] == 8 &&
        testdata[3] == 2 &&
        testdata[4] == 4 &&
        testdata[5] == 1;
    if(correct)
        std::cout << "passed test" << std::endl;
    else
        std::cout << "not passed test" << std::endl;
}

void testPrepB(void){
    int testdata[] = {5, 4, 8, 2, 9, 1, 7};
    prepareHeap(testdata, 7);

    bool correct =
        testdata[0] == 9 &&
        testdata[1] == 5 &&
        testdata[2] == 8 &&
        testdata[3] == 2 &&
        testdata[4] == 4 &&
        testdata[5] == 1 &&
        testdata[6] == 7;
    if(correct)
        std::cout << "passed test" << std::endl;
    else
        std::cout << "not passed test" << std::endl;
}

void testHeapifyA(void){
    int testdata[] = {1,5,8,2,4,9};
    heapify(testdata, 5);
    
    bool correct =
        testdata[0] == 8 &&
        testdata[1] == 5 &&
        testdata[2] == 1 &&
        testdata[3] == 2 &&
        testdata[4] == 4 &&
        testdata[5] == 9;
    if(correct)
        std::cout << "passed test" << std::endl;
    else
        std::cout << "not passed test" << std::endl;
}

void testHeapifyB(void){
    int testdata[] = {7,5,8,2,4,1,9};
    heapify(testdata, 6);

     bool correct =
        testdata[0] == 8 &&
        testdata[1] == 5 &&
        testdata[2] == 7 &&
        testdata[3] == 2 &&
        testdata[4] == 4 &&
        testdata[5] == 1 &&
        testdata[6] == 9;
    if(correct)
        std::cout << "passed test" << std::endl;
    else
        std::cout << "not passed test" << std::endl;
}

void testSortA(void){
    int testdata[] = {5, 4, 8, 2, 9, 1};
    heapsort(testdata, 6);
    
    bool correct =
        testdata[0] == 1 &&
        testdata[1] == 2 &&
        testdata[2] == 4 &&
        testdata[3] == 5 &&
        testdata[4] == 8 &&
        testdata[5] == 9;
    if(correct)
        std::cout << "passed test" << std::endl;
    else
        std::cout << "not passed test" << std::endl;
}

void testSortB(void){
    int testdata[] = {5, 4, 8, 2, 9, 1, 7};
    heapsort(testdata, 7);

     bool correct =
        testdata[0] == 1 &&
        testdata[1] == 2 &&
        testdata[2] == 4 &&
        testdata[3] == 5 &&
        testdata[4] == 7 &&
        testdata[5] == 8 &&
        testdata[6] == 9;
    if(correct)
        std::cout << "passed test" << std::endl;
    else
        std::cout << "not passed test" << std::endl;
}