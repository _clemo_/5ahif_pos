#pragma once
#define getLeavIdx(idx) ((idx<<1)+1) // a << 1 verschiebt a um ein bit nach links = multiplikation mit 2
#define getRootIdx(idx) ((idx-2)>>1) // a >> 1 verschiebt a um ein bit nach rechts = division mit 2
#define isNodeOk(arr, t, r, l) (arr[t]>arr[r] && arr[t]>arr[l]) // linkes und rechtes kind element müssen kleiner als das darüberliegende (t) sein
#define isOdd(var) ((var&0x1)>0)   // var&01 holt den wert des letzten bits (wenn 1 dann ungerade)

void swap(int arr[], int idxA, int idxB);
void prepareHeap(int arr[], int arr_size);
void print(int arr[], int arr_size);
void heapsort(int arr[], int arr_size);
void heapify(int arr[], int arr_size);