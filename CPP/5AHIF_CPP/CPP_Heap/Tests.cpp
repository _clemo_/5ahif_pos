#include "Tests.h" // implmentiert die hier definierten funktionen
#include <iostream>

void testPrepA(void){
    int testdata[] = {5, 4, 8, 2, 9, 1};
    prepareHeap(testdata, 6);

    bool correct =
        testdata[0] == 9 &&
        testdata[1] == 5 &&
        testdata[2] == 8 &&
        testdata[3] == 2 &&
        testdata[4] == 4 &&
        testdata[5] == 1;
    if(correct)
        std::cout << "passed test" << std::endl;
    else
        std::cout << "not passed test" << std::endl;
}

void testPrepB(void){
    int testdata[] = {5, 4, 8, 2, 9, 1, 7};
    prepareHeap(testdata, 7);

    bool correct =
        testdata[0] == 9 &&
        testdata[1] == 5 &&
        testdata[2] == 8 &&
        testdata[3] == 2 &&
        testdata[4] == 4 &&
        testdata[5] == 1 &&
        testdata[6] == 7;
    if(correct)
        std::cout << "passed test" << std::endl;
    else
        std::cout << "not passed test" << std::endl;
}

void testHeapifyB(void){
    int testdata[] = {7,5,8,2,4,1,9};
    heapify(testdata, 6);
    print(testdata, 7);
}