#pragma once
#include <iostream>
#include "heapsort.h" // implementiert die hier definierten funkionen

#define processNode(arr, t, l, r)                           \
        if(!isNodeOk(arr,t,r,l)){ /* wenn knoten nicht ok */\
            fixNode(arr, t, l, r);/* reperiere knoten     */\
        }

void swap(int arr[], int idxA, int idxB){
    int tVal = arr[idxA];
    arr[idxA] = arr[idxB];
    arr[idxB] = tVal;
}

void fixNode(int arr[], int t, int l, int r){
    int swapWith = l;                           // index des Nachfolgers
    if(l != r){
        swapWith = arr[r] > arr[l] ? r : l;     // w�hle gr��ten Nachfolger
    }
    swap(arr, t, swapWith);                     // tausche mit gew�hltem Nachfolger
}

void prepareHeap(int arr[], int arr_size){
    int rootIdx=0, leftIdx=0, rightIdx=0;

    for(int i = arr_size-1; i > 0; --i){
        if(isOdd(i)){ // erster durchlauf/ blatt nicht vollst�ndig
            rootIdx = getRootIdx(i+1);
            leftIdx = i;
            rightIdx = i; // einfache m�glichkeit bestehenden code an nur einen nachfolger anzupassen
        }else{ // blatt vollst�ndig
            rootIdx = getRootIdx(i);
            rightIdx = i;
            leftIdx = --i;
        }

        processNode(arr, rootIdx, leftIdx, rightIdx); // verarbeitet den aktullen Knoten vom Baum
    }
}

void heapify(int arr[], int arr_size){
    int rootIdx = 0, leftIdx = 0, rightIdx = 0;
    for(int i = 0; i < arr_size; ++i){
        rootIdx = i;
        leftIdx = getLeavIdx(i);
        rightIdx = leftIdx+1;

        if(rightIdx >= arr_size) rightIdx = leftIdx; // nur ein nachfolger au�erhalb
        if(leftIdx >= arr_size) return;              // beide nachfolger au�erhalb > fertig
        
        processNode(arr, rootIdx, leftIdx, rightIdx); // verarbeitet den aktullen Knoten vom Baum
    }
}

void heapsort(int arr[], int arr_size){
    prepareHeap(arr, arr_size);
    bool firstRun = true;

    for(int i = arr_size-1; i > 1; --i){ // i>1: letztes element ist immer sortiert
        if(!firstRun){                   // wenn baum inkonstent
            heapify(arr,i);              // reperiere baum
        }else{                           // erster durchlauf: baum ok
            firstRun = false;
        }

        swap(arr, i, 0);                 // gr��ten knoten nach hinten
    }
}

void print(int arr[], int arr_size){
    for(int i = 0; i < arr_size; ++i)
        std::cout << arr[i] << " ";
    std::cout<<std::endl;
}