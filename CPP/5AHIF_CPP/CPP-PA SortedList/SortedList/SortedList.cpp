// SortedList.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ListElement.h"
#include <iostream>

using namespace std;

/*void add(ListElement** root, int value) // jung
{
	ListElement* t = new ListElement;
	t->data = value;
	t->next = NULL;

	if(*root == NULL || (*root)->data > value){
		t->next = *root;
		*root = t;
		return;
	}
	ListElement* i = *root;
	while(i->next != NULL && i->next->data < value)
		i = i->next;
	t->next = i->next;
	i->next = t;
}*/

void add(ListElement** root, int value) { //kalenda
	ListElement* h = 0; 
	if(*root == 0 || (*root)->data > value) { 
		h = new ListElement;
		h->data = value; 
		h->next = (*root); 
		(*root) = h; 
	} else { 
		h = (*root); 
		while(h->next != 0 && h->next->data < value) 
			h = h->next; 
		ListElement* neu = new ListElement(); 
		neu->next = h->next; 
		neu->data = value; 
		h->next = neu; 
	} 
}

void addRek(ListElement** root, int value)
{
	if((*root) == NULL || (*root)->data > value){
		ListElement* t = new ListElement;
		t->data = value;
		t->next = *root;
		*root = t;
	}else{
		addRek(&((*root)->next), value);
	}
}

/*void addRek(ListElement** liste, int val) // friedl
{
 //ListenElement* h = *liste;

 if((*liste) == 0)
 {
  *liste = new ListElement;
  (*liste)->data = val;
  (*liste)->next = 0;
  
 }
 else
 {
  if((*liste)->data < val)
  {
   addRek(&(*liste)->next,val);
  }
  else
  {
   ListElement* einf = new ListElement;
   einf->data = val;
   einf->next = (*liste);
   (*liste) = einf;
  }
 }
}*/


void killList(ListElement* root)
{
	if(root == NULL) return;
	if(root->next == NULL){
		delete root;
		return;
	}
	ListElement*i = root->next;
	while(i != NULL){
		delete root;
		root = i;
		i = i->next;
	}
	delete root;
}

void killListRek(ListElement* root)
{
	if(root == NULL) return;
	killListRek(root->next);
	delete root;
}

void printIterativ(ListElement* root)
{
	ListElement* i = root;
	while (i != NULL)
	{
		std::cout << i->data << " ";
		i = i->next;
	}
}

void printRekursiv(ListElement* root)
{
	if(root == NULL) return;
	std::cout << root->data << " ";
	if(root->next != NULL){
		printRekursiv(root->next);
	}
}

void printRekursivReverse(ListElement* root)
{
	if(root == NULL) return;
	if(root->next != NULL){
		printRekursivReverse(root->next);
	}
	std::cout << root->data << " ";
}

int main()
{
	ListElement* root = 0;

	add(&root, 110);
	add(&root, 115);
	add(&root, 120);
	add(&root, 15);
	add(&root, 130);

	addRek(&root, 10);
	addRek(&root, 15);
	addRek(&root, 20);
	addRek(&root, 5);
	addRek(&root, 30);

	cout << "Ausgabe iterativ aufsteigend" << endl;
	printIterativ(root);
	cout << endl;

	cout << "Ausgabe rekursiv aufsteigend" << endl;
	printRekursiv(root);
	cout << endl;

	cout << "Ausgabe rekursiv absteigend" << endl;
	printRekursivReverse(root);
	cout << endl;

	cout << "Freigabe der Liste" << endl;
	//killListRek(root);
	killList(root);
	cout << endl;

	return 0;
}


