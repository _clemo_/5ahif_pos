#include "heapsort.h"
#include <iostream>

#define getParent(idx) ((idx-1)/2)
#define getChild(idx)  (idx*2+1)
#define isOdd(idx) (idx%2==1)
#define isNodeOk(arr, t, l, r) (arr[t] > arr[l] && arr[t] > arr[r])

#define max(arr, a, b) (arr[a] > arr[b] ? a : b)

void print(int arr[], int arr_size){
    for(int i = 0; i < arr_size; ++i)
        std::cout << arr[i] << " ";
    std::cout<<std::endl;
}

void swap(int arr[], int idxA, int idxB){
	int t = arr[idxA];
	arr[idxA] = arr[idxB];
	arr[idxB] = t;
}

void prepareHeap(int* arr, int arrLen){
	int top, left, right;
	for(int i = arrLen-1; i>= 0; --i){
		top = getParent(i);
		left = getChild(top);
		right = left+1;
		if(right > i)
			right = i;
		if(!isNodeOk(arr, top, left, right)){
			swap(arr, top, max(arr, left, right));
		}
	}
}

void shiftDown(int* arr, int max){
	int top, left, right;
	for(int i = 0; i < max; ++i){
		top = i;
		left = getChild(i);
		right = left+1;
		if(left >= max) break;
		if(right >= max)
			right = left;

		if(!isNodeOk(arr, top, left, right)){
			swap(arr, top, max(arr, left, right));
		}
		
	}
}

void heapsort(int* arr, int arrLen){
	prepareHeap(arr, arrLen);
	for(int i = 0; i < arrLen; ++i){
		shiftDown(arr, arrLen-i);
		swap(arr, 0, arrLen-(1+i));
	}
}