#pragma once
void swap(int* arr, int idxA, int idxB);
void prepareHeap(int* arr, int arrLen);
void shiftDown(int* arr, int max);
void heapsort(int* arr, int arrLen);
void print(int arr[], int arr_size);