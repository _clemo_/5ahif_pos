#pragma once
class Versicherungsmodell
{
public:
    char* getName(void);
    void setName(char* name);
    char* getSozVersNum(void);
    bool setSozVersNum(char* sozVersNum);
    Versicherungsmodell(void);
    ~Versicherungsmodell(void);
    static Versicherungsmodell* GetVers(char* name, char* sozvers);
    virtual double getJahresPraemie(void);
    virtual double getVerguetung(int khTage);
    virtual char* print(void);

protected:
    static bool isVersNumValid(char* num);

private:
    char* Name;
    char* SozNr;
    static int getVal(char c);
};

