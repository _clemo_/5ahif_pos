// CPP_KraVer.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "Kartei.h"
#include <iostream>

#include "VersicherungsmodellB.h"
#include "VersicherungsmodellA.h"
#include "Versicherungsmodell.h"


bool Test1(){ // fill and validation von Kartei
    Kartei k = Kartei(5);
    if(!k.addVers("test1","333",12,13)) return false;
    if(!k.addVers("test2","99",11,9)) return false;
    if(!k.addVers("test3","9333",11,9)) return false;
    if(k.addVers("test4","9334",11,9)) return false; // svznr = invalid
    if(!k.addVers("test4","93321",11,9)) return false;
    if(!k.addVers("test5","93339",11,9)) return false;
    return !k.addVers("test6","333",12,13);
}

bool Test2(){ // validierung
    Kartei k = Kartei(5);
    if(k.addVers("test","123456",10)) return false;
    if(!k.addVers("test2","123456789",10)) return false;
    return true;
}


#define testStringEqTest(v) if(v[0] != 't' || v[1] != 'e' || v[2] != 's' || v[3] != 't'){ return false;}
#define testStringEq333(v) if(v[0] != '3' || v[1] != '3' || v[2] != '3'){ return false;}

bool Test3(){ // string referenzen
    Versicherungsmodell v = *(Versicherungsmodell::GetVers("test","333"));
    char* nameV = v.getName();
    char* versNumV = v.getSozVersNum();
    testStringEqTest(nameV);
    testStringEq333(versNumV);

    Versicherungsmodell v2 = *(VersicherungsmodellA::GetVers("test","333", 1337));
    char* nameV2 = v2.getName();
    char* versNumV2 = v2.getSozVersNum();
    testStringEqTest(nameV2);
    testStringEq333(versNumV2);

    Versicherungsmodell v3 = *(VersicherungsmodellB::GetVers("test","333", 13, 37));
    char* nameV3 = v3.getName();
    char* versNumV3 = v3.getSozVersNum();
    testStringEqTest(nameV3);
    testStringEq333(versNumV3);

    return true;
}

bool Test4(){ // string referenzen bei concat
    Versicherungsmodell v = *(VersicherungsmodellA::GetVers("test","9",1337));
    char* info = v.print();
    testStringEqTest(info);
    Versicherungsmodell v2 = *(VersicherungsmodellB::GetVers("test","9",13,37));
    char* info2 = v.print();
    testStringEqTest(info2);
    return true;
}

bool Test5(){ // inheritence and singelton behaviour
    Kartei k = Kartei(5);
    if(!k.addVers("abc","999",400)) return false;
    if(k.getVerguetung("abc",10) != 100) return false;
    //(jahrenspraemie/40.0)*khTage = 400/40=10*10 = 100
    if(k.getVerguetung("abcd",10) != -1) return false;

    if(!k.addVers("abcd","9999", 30,5)) return false;
    if(k.getVerguetung("abcd",5) != 10) return false;
    // ersten 10 tage: (((Monatspraemie * 12) + Grundpraemie) / 45.0)*kh
    // 5*12 = 60 + 30 = 90/45 = 2 * 5 = 10
    if(k.addVers("abcd","9999", 3,5)) return false; // name already in use!!!

    if(!k.addVers("abcde","9999", 3,5)) return false;
    // 60+3 = 63/45 = 1,4 *5
    if(k.getVerguetung("abcde",5) != 7) return false;

    return true;
}

bool Test6(){
    Kartei k = Kartei(5);
    char* tmp = k.SubStr("x333;abc",1,4);
    testStringEq333(tmp);
    return true;
}

bool Test7(){
    Kartei k = Kartei(5);
    std::vector<char*>* tmp = k.SplitString("test;test;test",';');
    if(tmp->size() != 3) return false;
    for(int i = 0; i < 3; ++i){
        testStringEqTest(tmp->at(i));
    }
    return true;
}


int main(){
    if(Test1())
        printf_s("Test 1 passed!\n");
    else
        printf_s("Test 1 failed!\n");

    if(Test2())
        printf_s("Test 2 passed!\n");
    else
        printf_s("Test 2 failed!\n");
    
    if(Test3())
        printf_s("Test 3 passed!\n");
    else
        printf_s("Test 3 failed!\n");

    if(Test4())
        printf_s("Test 4 passed!\n");
    else
        printf_s("Test 4 failed!\n");

    if(Test5())
        printf_s("Test 5 passed!\n");
    else
        printf_s("Test 5 failed!\n");

    if(Test6())
        printf_s("Test 6 passed!\n");
    else
        printf_s("Test 6 failed!\n");

    if(Test7())
        printf_s("Test 7 passed!\n");
    else
        printf_s("Test 7 failed!\n");


	Kartei k = Kartei(5);
	k.readFromFile("KraVersTest.txt");
	k.printInfo();

    int t;
    std::cin >> t;
}


/*int _tmain(int argc, _TCHAR* argv[])
{
	return 0;
}*/
