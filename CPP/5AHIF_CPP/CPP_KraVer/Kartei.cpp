#include "Kartei.h"
#include "VersicherungsmodellA.h"
#include "VersicherungsmodellB.h"
#include <iostream>
#include <fstream>
#include <string>

Kartei::Kartei(void)
{
    Kartei(10);
}

Kartei::Kartei(unsigned int maxVersicherte)
{
    anzahl = maxVersicherte;
}


Kartei::~Kartei(void)
{
    for(int i = 0, max = Versicherte.size(); i < max; ++i){
        delete Versicherte[i];
    }
}

bool Kartei::addVers(char* name, char* svznr, int jp)
{
    if (Versicherte.size() >= anzahl) return false;
    if(GetVers(name) != 0) return false;

    Versicherungsmodell* val = VersicherungsmodellA::GetVers(name, svznr, jp);
    if(val == 0) return false;
    Versicherte.push_back(val);
    return true;
}

bool Kartei::addVers(char* name, char* svznr, double gp, double mp){
    unsigned int size = Versicherte.size();
    if (size >= anzahl) return false;
    if(GetVers(name) != 0) return false;

    Versicherungsmodell* val = VersicherungsmodellB::GetVers(name, svznr, gp, mp);
    if(val == 0) return false;
    Versicherte.push_back(val);
    return true;
}

void Kartei::printInfo()
{
	for(int i = 0, max = Versicherte.size(); i < max; ++i){
		// cout funtzt iwi bei ned :/
			//std::cout << Versicherte.at(i)->print() << std::endl;
		// >>> zu den c-wurzeln mit printf, da sehen nur manche buchstaben schei�e aus - funktioniert aber!
		printf(Versicherte.at(i)->print());
		printf("\r\n");
    }

}

Versicherungsmodell* Kartei::GetVers(char* name) // Sucht Versicherten mit Namen aus der Liste.
{
    Versicherungsmodell* c;
    for(int i = 0, max = Versicherte.size(); i < max; ++i){
        if(std::strcmp(name, (c=Versicherte.at(i))->getName()) == 0)
            return c;
    }
    return 0;
}

double Kartei::getVerguetung(char* name, int khTage){
    Versicherungsmodell* person = GetVers(name);
    if(person == 0)
        return -1;
    return person->getVerguetung(khTage);
}

bool Kartei::setSozVersNr(char* name, char* s){
    Versicherungsmodell* person = GetVers(name);
    if(person == 0)
        return false;
    char* oldSozVers = person->getSozVersNum();
    if(!person->setSozVersNum(s)){
        person->setSozVersNum(oldSozVers);
        return false;
    }
    return true;
}

void Kartei::reset(){
    Versicherte.resize(0); // muss nicht einzeln gel�scht werden da stack
}

char* Kartei::SubStr(const char* line, int begin, int end){
    int len = end - begin;
    char* ret = new char[len+1];
    for(int i = 0; i < len; ++i){
        ret[i] = line[begin+i];
    }
    ret[len] = '\0';
    return ret;
}

std::vector<char*>* Kartei::SplitString(const char* line, char split){
    using namespace std;
    vector<char*>* ret = new vector<char*>;
    int lineLen = strlen(line);

    int begin = 0;
    
    for(int i = 0; i < lineLen; ++i){
        if(line[i] == split){
            ret->push_back(SubStr(line, begin, i));
            begin = i+1;
        }
    }
	if(begin != lineLen)
		ret->push_back(SubStr(line, begin, lineLen));

    return ret;
}

bool Kartei::processLine(const char* line){
	using namespace std;
	vector<char*>* parts = SplitString(line, ';');
	char* name = parts->at(1);
	char* soz = parts->at(2);
	if(parts->at(0)[0] == 'A'){
		int jp = atoi(parts->at(3));
		if(!addVers(name, soz,jp)) return false;
	}else if(parts->at(0)[0] == 'B'){
		double gp = atof(parts->at(3));
		double mp = atof(parts->at(4));
		if(!addVers(name, soz, gp, mp)) return false;
	}else{
		return false;
	}
	return true;
}

void Kartei::readFromFile(char* fileName){
	using namespace std;
	string line;
	const char*  c_line;
	ifstream input;
	input.open(fileName);

	while(!input.eof())
	{
		std::getline(input, line);
		c_line = line.c_str();
		if(line.length() > 0){
			if(!processLine(c_line)) return;
		}
	}

}