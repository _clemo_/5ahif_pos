#pragma once
#include "versicherungsmodell.h"
class VersicherungsmodellB : public Versicherungsmodell
{
private:
    double Grundpraemie;
    double Monatspraemie;
    VersicherungsmodellB(void);

public:
    ~VersicherungsmodellB(void);
    double getJahresPraemie(void);
    double getVerguetung(int khTage);
    char* print(void);
    static VersicherungsmodellB* GetVers(char* Name, char* svnr, double Grundpraemie, double Monatspraemie);
};

