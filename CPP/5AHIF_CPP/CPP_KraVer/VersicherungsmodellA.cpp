#include "VersicherungsmodellA.h"
#include <iostream>

VersicherungsmodellA::VersicherungsmodellA(void)
{
}


VersicherungsmodellA::~VersicherungsmodellA(void)
{
}

Versicherungsmodell* VersicherungsmodellA::GetVers(char* name, char* sozvers, int Jahrspraemie){
    if(Jahrspraemie > 0 && Versicherungsmodell::isVersNumValid(sozvers)){
        VersicherungsmodellA* vm = new VersicherungsmodellA;
        vm->setName(name);
        vm->setSozVersNum(sozvers);
        vm->jahrenspraemie = Jahrspraemie;
        return vm;
    }else
        return 0;
}

double VersicherungsmodellA::getJahresPraemie(){
    return jahrenspraemie;
}

double VersicherungsmodellA::getVerguetung(int khTage){
    return (jahrenspraemie/40.0)*khTage;
}

char* VersicherungsmodellA::print(){
    char ret[1000];
    char* prev = Versicherungsmodell::print();
    sprintf_s(ret,"%s, Praemie/Jahr: %d", prev, jahrenspraemie);
    return ret;
}