#include "VersicherungsmodellB.h"
#include <iostream>

VersicherungsmodellB::VersicherungsmodellB(void)
{
}


VersicherungsmodellB::~VersicherungsmodellB(void)
{
}

VersicherungsmodellB* VersicherungsmodellB::GetVers(char* Name, char* svnr, double Grundpraemie, double Monatspraemie){
    if(Grundpraemie > 0 && Monatspraemie > 0 && Versicherungsmodell::isVersNumValid(svnr)){
        VersicherungsmodellB* vm = new VersicherungsmodellB;
        vm->setName(Name);
        vm->setSozVersNum(svnr);
        vm->Grundpraemie = Grundpraemie;
        vm->Monatspraemie = Monatspraemie;
        return vm;
    }else
        return 0;
}

double VersicherungsmodellB::getJahresPraemie(){
    return (Monatspraemie * 12) + Grundpraemie;
}

double VersicherungsmodellB::getVerguetung(int khTage)
{
    double verg = 0;
    double jp = getJahresPraemie();

    if (khTage <= 0) return verg; // falls negative Werte �bergeben wurden

    //ersten 10
    verg += (khTage < 10 ? khTage : 10) * (jp / 45.0);
    khTage -= 10;
    if (khTage <= 0) return verg;

    // 10-20
    verg += (khTage < 10 ? khTage : 10) * (jp / 40.0);
    khTage -= 10;
    if (khTage <= 0) return verg;

    //rest
    verg += khTage*(jp / 35.0);
    return verg;
}

char* VersicherungsmodellB::print(){
    char ret[1000];
    char* prev = Versicherungsmodell::print();
	float p = getJahresPraemie(); // achtumng: implizierter double>float cast!
    sprintf_s(ret,"%s, Praemie/Jahr: %f", prev, p);
    return ret;
}