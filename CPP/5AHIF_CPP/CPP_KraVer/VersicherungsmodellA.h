#pragma once
#include "versicherungsmodell.h"
class VersicherungsmodellA : public Versicherungsmodell
{
private:
    int jahrenspraemie;
    VersicherungsmodellA(void);

public:
    ~VersicherungsmodellA(void);
    double getJahresPraemie(void);
    double getVerguetung(int khTage);
    char* print(void);
    static Versicherungsmodell* GetVers(char* name, char* sozvers, int Jahrspraemie);
};

