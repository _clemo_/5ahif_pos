#pragma once
#include "Versicherungsmodell.h"
#include <vector>

class Kartei
{
private:
    unsigned int anzahl;
    int inUse;
    std::vector<Versicherungsmodell*> Versicherte;
    Versicherungsmodell* GetVers(char* name);
	bool processLine(const char* line);

public:
    Kartei();
    Kartei(unsigned int maxVersicherte);
    ~Kartei(void);
    bool addVers(char* name, char* svznr, int jp);
    bool addVers(char* name, char* svznr, double gp, double mp);
    void printInfo(void);
    double getVerguetung(char* name, int khTage);
    bool setSozVersNr(char* name, char* s);
    void reset();
	void readFromFile(char* fileName);

    std::vector<char*>* SplitString(const char* line, char split);
    char* SubStr(const char* line, int begin, int end);
};

/****************** WARUM IMMER POINTER??? ***************************
 *                                                                   *
 * Da ich mehrfach gefragt wurde warum an folgenden Stellen          *
 *  Pointer verwendet werden, hier die Erkl�rung.                    *
 *    ~ Kartei: vector<Versicherungsmodell*> Versicherte;            *
 *	  ~ Kartei: Versicherungsmodell* GetVers(char* name);            *
 *    ~ Versicherungsmodell: Versicherungsmodell* GetVers...         *
 *                                                                   *
 * Das hat mit der memory-allocation zu tun. Wenn wir den            *
 *  Speicher-Verbracuh der 3 Vers(icherungsmodell)                   *
 *  Implementierungen ansehen (vereinfacht als Bl�cke):              *
 *      +-------------+  +---------------+  +---------------+        *
 *      |     Vers    |  |     VersA     |  |    VersB      |        *
 *      +~~~~~~~~~~~~~+  +~~~~~~~~~~~~~~~+  +~~~~~~~~~~~~~~~+        *
 *      | Name        |  | Name          |  | Name          |        *
 *      +- - - - - - -+  +- - - - - - - -+  +- - - - - - - -+        *
 *      | SozVers     |  | SozVers       |  | SozVers       |        *
 *      +-------------+  +- - - - - - - -+  +- - - - - - - -+        *
 *                       | Jahrespr�mie  |  | GrundPr�mie   |        *
 *                       +---------------+  +- - - - - - - -+        *
 *                                          | MonatsPr�mie  |        *
 *                                          +---------------+        *
 *                                                                   *
 * wenn jetzt ein array der Gr��e n vom Typ Vers angelegt wird,      *
 *  so wird n mal die Gr��e von Vers reserviert.                     *
 *  Wenn man jetzt versuchen w�rde VersA oder VersB hinein zu        *
 *  schreiben - sollte jedem auffallen dass                          *
 *     n*sizeof(Vers) < n*sizeof(VersA)                              *
 *                  und                                              *
 *     n*sizeof(Vers) < n*sizeof(VersB)                              *
 * Um solche AccessViolation's zu vermeiden legt man Pointer an      *
 * denn die Pointer auf Klassen sind ALLE gleich gro�!               *
 *  Grund: unter 32 Bit Windows sind Pointer immer 32 Bit gro�,      *
 *         unter 64 Bit Windows sind Pointer immer 64 Bit gro�       *
 *  da ein Pointer ja nur eine Speicheradresse im RAM ist und        *
 *   die "Bit's" von Windows geben an mit wie vielen Bits die        *
 *   Speicheradressen adressiert werden.                             *
 * daraus folgt                                                      *
 *  sizeof(Vers*) = sozeof(VersA*) = sizeof(VersB*)                  *
 * So kann man ein Array von Vers* anlegen und auf VersA und         *
 *  VersB Objekte zeigen lassen ohne Probleme.                       *
 *********************************************************************/