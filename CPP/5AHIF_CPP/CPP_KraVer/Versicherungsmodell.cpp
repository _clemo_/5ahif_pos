#include "Versicherungsmodell.h"
#include <iostream>

Versicherungsmodell::Versicherungsmodell(void)
{
}


Versicherungsmodell::~Versicherungsmodell(void)
{
}

Versicherungsmodell* Versicherungsmodell::GetVers(char* name, char* sozvers){
    if(isVersNumValid(sozvers)){
        Versicherungsmodell* vm = new Versicherungsmodell;
        vm->setName(name);
        vm->setSozVersNum(sozvers);
        return vm;
    }else
        return 0;
}

int Versicherungsmodell::getVal(char c){
	// stattdessen k�nnte std::atoi(&c) benutzt werden!
	// oder c -'0'
    switch (c)
    {
        case '0':
            return 0;
        case '1':
            return 1;
        case '2':
            return 2;
        case '3':
            return 3;
        case '4':
            return 4;
        case '5':
            return 5;
        case '6':
            return 6;
        case '7':
            return 7;
        case '8':
            return 8;
        case '9':
            return 9;
        default:
            return -1;
    }
    return -1;
}

bool Versicherungsmodell::isVersNumValid(char* num){
    int t;
    int sum = 0;

    int i = 0;
    char temp;
    while((temp=num[i++]) != '\0'){
        t = getVal(temp);
        if(t != -1)
            sum += t;
        else 
            return false;
    }
    return (sum % 9 == 0);
}

bool Versicherungsmodell::setSozVersNum(char* sozVersNum){
    if(isVersNumValid(sozVersNum)){
        SozNr = sozVersNum;
        return true;
    }else
        return false;
}

char* Versicherungsmodell::getSozVersNum(){
    return SozNr;
}

char* Versicherungsmodell::getName(){
    return Name;
}

void  Versicherungsmodell::setName(char* name){
    Name = name;
}

double Versicherungsmodell::getJahresPraemie(){
    return 0;
}

double Versicherungsmodell::getVerguetung(int khTage){
    return 0;
}

char* Versicherungsmodell::print(){
    return Name;
}