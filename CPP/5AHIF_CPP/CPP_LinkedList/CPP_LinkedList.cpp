#include "stdafx.h"


#include <iostream>

struct ListItem{
	int data;
	ListItem* next;
};

ListItem* add_unsorted(ListItem* list, int val){
	ListItem* o = new ListItem;
	o->data = val;
	o->next = list;
	return o;
}

/*********** Sortiert einf�gen begin *************************/
// Variante mit Pointer als Eingabe und als R�ckgabe
ListItem* add_sorted(ListItem* list, int val){
	ListItem* obj = new ListItem;
	obj->data = val;
	obj->next = 0;
	if(!list) return obj;

	ListItem* c = list;
	

	while(c->next != 0 && c->next->data < val){ // solange es ein n�chstes gibt und das n�chste element kleiner ist
		c = c->next; // gehe weiter
	}

    // f�ge objekt zwischen c und c.next ein
	obj->next = c->next;
	c->next=obj;

	return list;
}

// Variante ohne R�ckgabe mit Pointer auf Pointer als Eingabe
void add_sorted_direct(ListItem** ptr_list, int val){
    ListItem* input = (*ptr_list);
    ListItem* tmp = add_sorted(input, val);
    (*ptr_list) = tmp;
}

// ERKL�RUNG
// Verkettete-Listen werden �bergeben indem die Adresse auf das 
// erste Element �bergeben wird
// Wenn das erste Element ge�ndert werden muss, so muss 
//  a) die Speicheradresse des ersten Elementes ge�ndert werden
//  b) am Ender der Funktion wird jeweils die (neue) Adresse des ersten Elementes zurr�ckgegeben
//
// Umsetzung a:
//  Man �bergibt nicht mehr ListItem* sondern ListItem**, Grund:
//    ListItem* ist eine Speicheradresse die nur read-only �bergeben wird. Damit die Speicheradresse
//     ge�ndert werden kann macht man einen Pointer auf die Speicheradresse.
//     Denn, dann muss man nicht mehr den Pointer selbst �ndern, sondern nur mehr dessen Wert 
//     (was wiederum ein Pointer ist)
//  Zugriff:
//     Um jetzt auf die Daten von Pointer auf Pointern zuzugreifen muss man z.B. so auf die Eigenschaft data zugreifen:
//      *(ptr_list)->data
// Umsetzung b:
//   Am Ende der Funktion immer den Pointer auf das erste Element der Liste zurr�ckgeben.
//   Dann braucht man auch nur einmal de-referenziereun und kann z.B.: ptr_list->data zugreifen.
/*********** Sortiert einf�gen end *************************/

void distinct(ListItem* l){ // l ist sortiert
    ListItem* c = l;
    while(c != 0){
        if(c->next){ // wenn n�chstes element verf�gbar
            if(c->data == c->next->data){ // aktuell.wert == n�chstes.wert
                ListItem* old = c->next;  // n�chstes = �berfl�ssiges element
                c->next=c->next->next;    // �berspringe n�chstes (aktuell.next = next.next)
                delete old;               // l�sche �berfl�ssiges element aus ram
            }else  // wenn n�chstes nicht gleich
                c = c->next; // gehe zum n�chsten element
        }else // kein weiteres element verf�gbar
            return; // fertig
    }
}

void print(ListItem* l){
	ListItem* tmp = l;
	while(tmp!=0){
		std::cout << tmp->data << std::endl;
		tmp = tmp->next;
	}
    std::cout << std::endl;
}

ListItem* reverse(ListItem* i){ 
    ListItem* n = new ListItem;         // Kopie von aktuellem wert
    n->data = i->data;                  
    n->next = 0;

    if(i->next != 0){                       // wenn n�chstes element verf�gbar
        ListItem* rev = reverse(i->next);   // reverse(nachfolgende)
        ListItem* last = rev;               // cursor f�r reversed
        while(last->next != 0)last=last->next;// suche letztes element
        last->next = n;                     // aktuelle eslement = letztes von reversed
        return rev;                         // gib pointer auf begin der reversed-list zur�ck
    }else{
        return n;                           // 1 element ist immer reversed!   
    }
}


ListItem* deleteFirst(ListItem* l){ // l�sche erstes element aus liste
    ListItem* old = l;
    ListItem* ret = l->next;
    delete old;
    return ret;
}

ListItem* deleteItem(ListItem* l, int idx){
    if(idx == 0)                    // erstes element
        return deleteFirst(l);      // l�sche erstes und gib adresse von 2tem element zur�ck

    ListItem* c = l;                // cursor zum iterieren
    for(int i = 1; i < idx; ++i)
        c = c->next;

    c->next = deleteFirst(c->next); // n�chstes = l�sche n�chstes
    return l;                       // gib adresse erstes element zur�ck
}

ListItem* add_atEnd(ListItem* l, int val){
    ListItem* obj = new ListItem;
    obj->data = val;
    obj->next = 0;
    if(!l)
        return obj;
    ListItem* c = l;
    while(c->next != 0)
        c = c->next;
    c->next = obj;
    return l;
}

ListItem* InsertAt(ListItem *head, int data, int position)
{
    ListItem* obj = new ListItem;
    obj->data = data;
    
    if(position == 0){
        obj->next = head;
        return obj;
    }
    
    ListItem* c = head;
    for(int i = 1; i < position; ++i){
        c = c->next;
    }
    obj->next = c->next;
    c->next = obj;
    return head;
}

ListItem* addSortedIterativkalle(ListItem* root, int value) 
{ 
	ListItem* h = new ListItem(); 
	if(root == 0 || root->data > value) 
	{ 
		h->data = value; 
		h->next = root; 
		root = h; 
	} else 
	{ 
		h = root; 
		while(h->next != 0 && h->next->data < value) 
			h = h->next; 
		ListItem* neu = new ListItem(); 
		neu->next = h->next;
		neu->data = value; 
		h->next = neu;
	} 
	return root; 
}

int main(){

	/*ListItem* liste = 0;
	liste = add_unsorted(liste, 7);
	liste = add_unsorted(liste,13);
	liste = add_unsorted(liste,1380);
	liste = add_unsorted(liste,8);
	liste = add_unsorted(liste,333);*/
	ListItem* liste = NULL;
	liste = addSortedIterativkalle(liste, 2);
	liste = addSortedIterativkalle(liste, 3);
	liste = addSortedIterativkalle(liste, 1);
	liste = addSortedIterativkalle(liste, 5);
	liste = addSortedIterativkalle(liste, 6);
	liste = addSortedIterativkalle(liste, 0);
	print(liste);
	int x;
	std::cin >> x;

	ListItem* liste2 = 0;
	add_sorted_direct(&liste2, 7);
    add_sorted_direct(&liste2,8);
	add_sorted_direct(&liste2,13);
    add_sorted_direct(&liste2,7);
	add_sorted_direct(&liste2,1380);
	add_sorted_direct(&liste2,8);
	add_sorted_direct(&liste2,333);
    add_sorted_direct(&liste2,8);

    //print(liste2);
    distinct(liste2);
    liste2 = add_sorted(liste2,5000);
    print(liste2);

    ListItem* rev = reverse(liste2);
    print(rev);

    /*ListItem* liste3 = 0;
    liste3 = add_sorted(liste3, 1);
    liste3 = add_sorted(liste3, 2);
    liste3 = add_sorted(liste3, 3);

    print(liste3);

    liste3 = deleteItem(liste3,2);

    print(liste3);*/
	std::cin >> x;
}

/*int _tmain(int argc, _TCHAR* argv[])
{
	return 0;
}*/

