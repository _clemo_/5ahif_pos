﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRYPT
{
    class Program
    {
        private static string charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
        static void Main(string[] args)
        {
            Console.Write("KLARTEXT: ");
            var text = Console.ReadLine();
            Console.WriteLine("Methoden: ");
            Console.WriteLine("\t1) Cäsar");
            Console.WriteLine("\t2) Vigenere");
            Console.WriteLine("\t3) Vigenere Decrypt");
            var method = Int32.Parse(Console.ReadLine());

            Func<string,int,string> caesar = (klatext, val) => {
                return String.Concat(from Item in klatext select charset[(charset.IndexOf(Item)+val)%charset.Length]);
            };

            Func<string, string, string> vigenere = (klartext, key) =>
            {
                StringBuilder sb = new StringBuilder(klartext.Length);
                for (int keyLen = key.Length, textLen = klartext.Length, i = 0; i < textLen; ++i)
                {
                    var keyIdx = (charset.IndexOf(key[i % keyLen]))+1;
                    var textIdx = (charset.IndexOf(klartext[i]))+1;
                    var newIDx = keyIdx + textIdx - 2;
                    sb.Append(charset[newIDx % charset.Length]);
                }
                return sb.ToString();
            };

            Func<string, string, string> vigenereDecrypt = (crypt, key) =>
            {
                StringBuilder sb = new StringBuilder(crypt.Length);
                for (int keyLen = key.Length, textLen = crypt.Length, i = 0; i < textLen; ++i)
                {
                    var keyIdx = (charset.IndexOf(key[i % keyLen])) + 1;
                    var textIdx = (charset.IndexOf(crypt[i])) + 1;
                    var newIDx = keyIdx - textIdx - 2 + charset.Length;
                    
                    sb.Append(charset[newIDx % charset.Length]);
                }
                return sb.ToString();
            };

            switch (method)
            {
                case 1:
                    Console.Write("Schlüssel WERT: ");    
                    var key = Int32.Parse(Console.ReadLine());
                    Console.WriteLine(caesar(text,key));
                    break;
                case 2:
                    Console.Write("Schlüssel WORT: ");    
                    var keyword = Console.ReadLine();
                    Console.WriteLine(vigenere(text, keyword));
                    break;
                case 3:
                    Console.Write("Schlüssel WORT: ");    
                    var keyword2 = Console.ReadLine();
                    Console.WriteLine(vigenereDecrypt(text, keyword2));
                    break;
                    
            }
            Console.ReadLine();
        }
    }
}
