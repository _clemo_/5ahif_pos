﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vector
{
    class Program
    {
        static void Main(string[] args)
        {
            Vector a = new Vector(3, 5);
            Vector b = new Vector(-3, 5);
            Console.WriteLine(a+"+"+b+"="+(a+b));
            Console.WriteLine(a + "-" + b + "=" + (a - b));
            Console.WriteLine(a + "*3=" + (a * 3));
            Console.WriteLine(a + "/3=" + (a / 3));
            Console.ReadLine();
        }
    }
}
