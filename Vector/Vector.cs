﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vector
{
    class Vector
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Vector(int X = 0, int Y= 0)
        {
            this.X = X;
            this.Y = Y;
        }

        public static Vector operator  + (Vector a, Vector b){
            return new Vector(a.X + b.X, a.Y + b.Y);
        }

        public static Vector operator -(Vector a, Vector b)
        {
            return new Vector(a.X - b.X, a.Y - b.Y);
        }

        public static Vector operator *(Vector a, int b)
        {
            return new Vector(a.X * b, a.Y * b);
        }

        public static Vector operator /(Vector a, int b)
        {
            return new Vector((int)Math.Round(a.X / (float)(b), 0), (int)Math.Round(a.Y / (float)(b), 0));
        }

        public static Vector operator <<(Vector a, int b)
        {
            return a*(int)Math.Pow(2,b);
        }

        public static Vector operator >>(Vector a, int b)
        {
            return a / (int)Math.Pow(2, b);
        }

        public static bool operator ==(Vector a, Vector b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Vector a, Vector b)
        {
            return !(a == b);
        }

        public override string ToString()
        {
            return "(" + X + "/" + Y + ")";
        }
    }
}
