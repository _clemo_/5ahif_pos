﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quicksort
{
    class Program
    {
        static List<int> QuickSort(List<int> inp)
        {
            if (inp.Count <= 1) return inp;
            int piv = inp[0];
            List<int> left = new List<int>(), right = new List<int>();
            for (int i = 1; i < inp.Count; ++i)
                if (inp[i] < piv)
                    left.Add(inp[i]);
                else
                    right.Add(inp[i]);
            left = QuickSort(left);
            right = QuickSort(right);

            left.Add(piv);
            foreach (var item in right)
                left.Add(item);
            return left;
        }

        static void Main(string[] args)
        {
            Console.WriteLine(
                String.Join(" ", QuickSort(new List<int>(new int[]{6,3,1,4,5,2, 0})))
            );
            Console.ReadLine();
        }
    }
}
