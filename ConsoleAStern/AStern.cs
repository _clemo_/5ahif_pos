﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAStern
{
    public partial class  AStern
    {
        private Dictionary<string, List<Verbindung>> conns = new Dictionary<string, List<Verbindung>>();
        private Dictionary<string, Punkt> nodes = new Dictionary<string, Punkt>();

        public AStern(List<Punkt> Nodes, List<Verbindung> Connections)
        {
            foreach (var item in Nodes) // übernimm alle Punkte in struktur bezeichnung>Punkt
            {
                nodes.Add(item.Bezeichnung, item);
            }
            foreach (var item in Connections) // übernimm alle verbindungen in struktur von/nach>verbindung
            {
                if (!conns.ContainsKey(item.VON))
                    conns.Add(item.VON, Verbindung.AlleVerbindungen[item.VON]);
                if (!conns.ContainsKey(item.NACH))
                    conns.Add(item.NACH, Verbindung.AlleVerbindungen[item.NACH]);
            }
        }

        private List<PossiblePoint> toPossiblePoints(Punkt from, double way, List<Verbindung> neighbours)
        {
            List<PossiblePoint> t = new List<PossiblePoint>();
            foreach (var item in neighbours)
            {
                t.Add(new PossiblePoint(from, item.Other(from.Bezeichnung), item, way));
            }

            return t;
        }

        public bool RoutePossible(string from, string to)
        {
            var target = nodes[to];

            Func<string, bool> isConnectedWithTarget = (t) =>
            {
                if (t == null || from == null || !conns.ContainsKey(t)) return false;
                foreach (var connection in conns[t])
                {
                    if (connection.VON.Equals(to) || connection.NACH.Equals(to))
                        return true;
                }
                return false;
            };

            Func<string, List<string>> PossibleRoutes = (n) =>
            {
                if (n == null || from == null || !conns.ContainsKey(n)) return new List<string>();
                var con = conns[n];
                var ret = new List<string>();
                Parallel.ForEach(con,
                    (c) =>
                    {
                        if (!ret.Contains(c.NACH))
                            ret.Add(c.NACH);
                        if (!ret.Contains(c.VON))
                            ret.Add(c.VON);
                    }
                );
                return ret;
            };

            List<string> nodesToCheck = new List<string>() { from };
            while (nodesToCheck.Count > 0)
            {
                var node = nodesToCheck[0];
                nodesToCheck.RemoveAt(0);
                if (isConnectedWithTarget(node)) return true;
                var connectedWith = PossibleRoutes(node);
                foreach (var item in connectedWith)
                {
                    if (!nodesToCheck.Contains(item))
                        nodesToCheck.Add(item);
                }
            }
            return false;
        }

        public List<Punkt> GetRoute(string from, string to)
        {
            string start = from;
            SortedSet<PossiblePoint> openlist = new SortedSet<PossiblePoint>(new PossiblePointComparer(nodes[to])); // sortiert baum mit PossiblePointComparer
            List<VisitedPoint> closelist = new List<VisitedPoint>();
            double way = 0.0; //zurückgelegter weg
            bool loop = true;
            do
            {
                if (!conns.ContainsKey(from)) return null; // wenn ausgangspunkt nicht vorhanden

                var temp = toPossiblePoints(nodes[from], way, conns[from]); // convertiert Nodes > PossiblePoints (rechnet Weg bis dahin dazu und speichert den Weg ab)
                foreach (var item in temp)
                {
                    if (!openlist.Contains(item)) // wenn der punkt noch nicht in der liste -> füge ihn hinzu
                        openlist.Add(item);
                }

                var tPoint = openlist.First();
                loop = openlist.Count > 0 && !tPoint.To.Bezeichnung.Equals(to); // prüft ob erster knoten aus der openList = Ziel

                way = tPoint.UpdatedWay; // zurückgelegter weg + länge der verbindung


                closelist.Add(new VisitedPoint(tPoint.From.Bezeichnung, way, tPoint.To));

                from = tPoint.To.Bezeichnung;
                openlist.Remove(tPoint);
            } while (loop);

            List<Punkt> resList = new List<Punkt>();
            while (to != start)
            {
                var tpt = closelist.Where(x => x.Point.Bezeichnung.Equals(to)).ToArray();
                resList.Insert(0, tpt[0].Point);
                to = tpt[0].From;
                closelist.Remove(tpt[0]);
            }
            resList.Insert(0, nodes[start]);
            return resList;
        }
        
        class VisitedPoint
        {
            public string From;
            public double Way; // weg bis zum punkt
            public Punkt Point { get; private set; }
            
            public VisitedPoint(string from, double way, Punkt point)
            {
                From = from;
                Way = way;
                Point = point;
            }

            public override string ToString() // fürs debuggen
            {
                return "[" + Punkt.AllePunkte[From] + " > " + Point + "; " + Way + "]";
            }
        }

        class PossiblePoint
        {
            public Punkt From { get; private set; }
            public Punkt To { get; private set; }
            public Verbindung Connection { get; private set; }
            public double UpdatedWay
            {
                get
                {
                    return WayToSource + Connection.LENGTH;
                }
            }

            private double? _expWayDelta = null; // luftline: ende der linie-ziel
            // double? ist kurz für Nullable<double> was ein Sprachkonstrukt für Werte ist die entweder null oder double ist.


            public double ExpectedWay(Punkt to) // schätzfunktion
            {
                if(_expWayDelta == null) // wenn noch nicht erechent > ausrechnen, sonst vorheriges ergbenis
                    _expWayDelta = (Math.Sqrt(Math.Pow(To.X - to.X, 2) + Math.Pow(To.Y - to.Y, 2))); // distanz: ende der line -> ziel
                return UpdatedWay + (double)_expWayDelta;// weg bisher + weglänge + luftlinie zum ziel
            }


            public PossiblePoint(Punkt from, Punkt to, Verbindung conn, double way)
            {
                From = from;
                To = to;
                Connection = conn;
                WayToSource = way;
            }

            public double WayToSource { get; private set; }

            public override string ToString() // ToString ist immer hilfreich zum debuggen!
            {
                return "{"+From+" > "+To+" : "+UpdatedWay + (_expWayDelta != null ?(" (exp: "+(UpdatedWay+_expWayDelta)+")"):"")+"}";
            }
        }

        class PossiblePointComparer : IComparer<PossiblePoint>
        {
            Punkt target;
            int IComparer<PossiblePoint>.Compare(PossiblePoint x, PossiblePoint y) // vergleicht die erwarteten Wege
            {
                return x.ExpectedWay(target).CompareTo(y.ExpectedWay(target));
            }

            public PossiblePointComparer(Punkt to) // speichert Punkt zum vergleichen ab (Ziel-Knoten)
            {
                target = to;
            }
        }
    }
}
