﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ConsoleAStern
{
    [Serializable()]
    public class Exchange
    {
        public List<Punkt> Punkte = null;
        public List<Verbindung> Verbindungen = null;
        public string ImgPath = null;
        public int AreaSize = 50;
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Punkt[] graphPoints = new Punkt[]{
                new Punkt(2, 7, "A"),
                new Punkt(7, 8, "B"),
                new Punkt(14, 6, "C"),
                new Punkt(13, 14, "D"),
                new Punkt(6, 3, "E"),
                new Punkt(10, 1, "F"),
                new Punkt(15, 1, "G"),
                new Punkt(20, 0, "H"),
                new Punkt(0, 11, "I"),
                new Punkt(20, 7, "Y"),
                new Punkt(18, 5, "Z")
            };

            Verbindung[] graphConnections = new Verbindung[]{
                new Verbindung("A","B"),
                new Verbindung("A","E"),
                new Verbindung("A","I"),
                new Verbindung("I","Y"), // not direct connected 
                new Verbindung("E","F"),
                new Verbindung("E","B"),
                new Verbindung("F","G"),
                new Verbindung("F","C"),
                new Verbindung("G","C"),
                new Verbindung("G","H"),
                new Verbindung("Z","H"),
                new Verbindung("C","Z"),
                new Verbindung("B","D"),
                new Verbindung("B", "C"),
                new Verbindung("D","Z")
            };

            // hacky way to multiply distance
            Verbindung.AlleVerbindungen["I"].Where(x => (x.NACH == "Y") || x.VON == "Y").ToArray()[0].LENGTH *= 4;

            var alg = new AStern(graphPoints.ToList(), graphConnections.ToList());
            var res = alg.GetRoute("A", "Z");
            Console.WriteLine(String.Join(", ", res));

            /*
            XmlSerializer xs = new XmlSerializer(typeof(Exchange));
            using (StreamWriter sw = new StreamWriter("exchange.xml"))
            {
                Exchange x = new Exchange(){Punkte = new List<Punkt>(graphPoints), Verbindungen = new List<Verbindung>(graphConnections)};
                xs.Serialize(sw, x);
                sw.Flush();
            }*/

            Console.ReadLine();
        }
    }
}
