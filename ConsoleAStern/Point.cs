﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleAStern
{
    [Serializable()]
    public partial class Punkt
    {  
        public static Dictionary<string, Punkt> AllePunkte = new Dictionary<string, Punkt>(); // speichert alle Punkte, benötigt damit verbindungen länge berechnen können
        public int X;
        public int Y;
        public string Bezeichnung; // sollte unique sein

        public Punkt(int X, int Y, string bez)
        {
            this.X = X;
            this.Y = Y;
            Bezeichnung = bez;
            AllePunkte.Add(Bezeichnung, this);
        }

        public Punkt() // fürs serialisieren/deserialisieren
        {

        }

        public override string ToString()  // fürs Debuggen
        {
            return Bezeichnung+"("+X+"/"+Y+")";
        }
    }

    [Serializable()]
    public partial class Verbindung
    {
        public static Dictionary<string, List<Verbindung>> AlleVerbindungen = new Dictionary<string, List<Verbindung>>();
        /**
         * Dictionary mit Ort und Verbindungen zu/von diesem Ort
         */

        public string VON;  // Bezeichnung von einem Punkt
        public string NACH; // Bezeichnung von einem Punkt

        private double? _len = null; // lazy loader, double? = double oder null
        public double LENGTH
        {
            get
            {
                if (_len == null) // wenn noch nicht berechnet: rechne
                {
                    Punkt a = Punkt.AllePunkte[VON], b = Punkt.AllePunkte[NACH]; // hohle Punkt-Objekte 
                    _len = Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));// rechne distanz aus
                }
                return (double)_len; // gib ergebnis zurück
            }

            set{
                _len = value;
            }
        }

        public static void _add(string key, Verbindung v) // fügt ort+verbinfung in die liste ein
        {
            if (AlleVerbindungen.ContainsKey(key)) // wenn listenobjekt angelegt
            {
                if (!AlleVerbindungen[key].Contains(v)) // wenn noch nicht enthalten
                    AlleVerbindungen[key].Add(v);       // füge hinzu
            }
            else
            {
                AlleVerbindungen.Add(key, new List<Verbindung>() { v }); // erzeuge neue liste mit Punkt
            }
        }

        public Verbindung(string Von, string Nach, double? Len = null)
        {
            VON = Von;
            NACH = Nach;
            _len = Len;

            _add(Von, this);
            _add(Nach, this);
        }

        public override string ToString() // fürs debuggen
        {
            return "{"+VON+"-"+NACH+": "+LENGTH+"}";
        }

        public Punkt Other(string a) // a = bezeichnugn von einem Punkt; gibt die Objekt-instanz des anderen Punktes zurrück
        {
            return a == VON ? Punkt.AllePunkte[NACH] : Punkt.AllePunkte[VON];
        }

        public Verbindung()
        {

        }
    }
}
