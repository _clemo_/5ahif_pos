﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collection
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable<string, string> table = new Hashtable<string, string>(100);
            int menge = 80;
            var rndKeys = GenerateRandomText(menge, 5);
            for (int i = 0; i < menge; i++)
            {
                table[rndKeys[i]] = i.ToString();
            }

            Console.WriteLine(table.Print());
            Console.ReadLine();
        }

        static Random r = new Random();
        static string charset = "0123456789qwertzuiopüasdfghjklöäyxcvbnm";
        static char RndChar()
        {
            int len = charset.Length;
            return charset[r.Next(len)];
        }

        static string[] GenerateRandomText(int amout, int length)
        {
            
            string[] ret = new string[amout];
            Parallel.For(0, amout, (i) =>
                {
                    StringBuilder sb = new StringBuilder(length);
                    for (int x = 0; x < length; ++x)
                    {
                        sb.Append(RndChar());
                    }
                    ret[i] = sb.ToString();
                }
            );
            return ret;
        }
    }


}
