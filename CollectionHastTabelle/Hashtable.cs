﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collection
{
    public class Hashtable<KeyType,ValueType> 
    {
        private List<KeyValuePair<KeyType, ValueType>>[] Data;
        private int Size;


        public Hashtable(int Size)
        {
            Data = new List<KeyValuePair<KeyType, ValueType>>[Size];
            this.Size = Size;
        }

        private UInt64 HashToString(KeyType key){ // methode um aus dem toString() einen hash zu generieren
            var s = key.ToString();

            byte[] data = Encoding.ASCII.GetBytes(s.ToCharArray());
            UInt64 res = 0;
            foreach (var b in data)
	        {
		        res *= 255;
                res += b;
	        }
            return res;
        }

        public ValueType this[KeyType k]{
            get{
                //falls hash funktion nicht erlaubt ist
                //UInt64 hash = HashToString(k);
                //int idx = Convert.ToInt32(hash % Convert.ToUInt64(Size));

                int idx = k.GetHashCode() % Size;
                if (idx < 0) idx = -idx;

                if (Data[idx] == null)
                    return default(ValueType);
                else
                {
                    var list = Data[idx];
                    if (list.Count == 0)
                        return default(ValueType);
                    else if (list.Count == 1)
                        return list[0].VALUE;
                    else
                    {
                        var erg = list.Where((x) => { return x.KEY.Equals(k); });
                        var ergList = erg.ToList();
                        if (ergList.Count <= 0)
                            return default(ValueType);
                        else
                            return ergList[0].VALUE;
                    }
                }
            }

            set{
                //falls hash funktion nicht erlaubt ist
                //UInt64 hash = HashToString(k);
                //int idx = Convert.ToInt32(hash % Convert.ToUInt64(Size));

                var item = new KeyValuePair<KeyType, ValueType>() { KEY = k, VALUE = value };
                int idx = k.GetHashCode() % Size;
                if (idx < 0) idx = -idx;

                if (Data[idx] == null) // key noch nie verwendet
                {
                    Data[idx] = new List<KeyValuePair<KeyType, ValueType>>() { item };
                }
                else // key bereits verwendet (list wurde instanziert)
                {
                    if (Data[idx].Count == 0) // hash noch nie genutzt?
                        Data[idx].Add(item);
                    else
                    {
                        var search = Data[idx].Where((x) => { return x.KEY.Equals(k); }); // prüft ob key in verwendung
                        if (search.Count() == 0) // mehrere einträge auf hash, key noch nicht benutzt
                        {
                            Data[idx].Add(item);
                        }
                        else // key ist in verwendung
                        {
                            var list = search.ToList();
                            if (list.Count == 1) // wenn nur ein objekt, dann den wert ändern
                            {
                                list[0].VALUE = value;
                            }
                            else // sollte nie vorkommen
                            {
                                throw (new ArgumentOutOfRangeException("Der verwendete Schlüssel ist nicht eindeutig!!!"));
                            }
                        }
                    }
                }
            }
        }


        public string Print()
        {
            StringBuilder sb = new StringBuilder();
            string line;
            for(int i = 0; i < Size; ++i)
            {
                line = "Zelle " + i.ToString() + "(" + (Data[i] == null ? 0 : Data[i].Count) + "): " + (Data[i] == null ? "-" : String.Join(", ", Data[i]));
                sb.AppendLine(line);
            }
            return sb.ToString();
        }

        private class KeyValuePair<key, val>
        {
            public key KEY { get; set; }
            public val VALUE { get; set; }

            public override string ToString()
            {
                return "{"+KEY.ToString()+": "+VALUE.ToString()+"}";
            } 
        }
    }
}
