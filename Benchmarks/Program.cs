﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Benchmarks
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            const int cycles = 100000000;

            sw.Start();
            for (int i = 0; i < cycles; i++)
                
            sw.Stop();

            var sw2 = new Stopwatch();
            sw2.Start();
            for (int i = 0; i < cycles; i++)
                
            sw2.Stop();

            Console.WriteLine("shift:"+sw.ElapsedMilliseconds);
            Console.WriteLine("mul: "+sw2.ElapsedMilliseconds);
            Console.ReadLine();
        }
    }
}
