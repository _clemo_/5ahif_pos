﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS
{
    class HashTabelle<K,V> 
    {
        ListElement<KeyValuePair<K, V>>[] Map;
        int Size;
        
        public HashTabelle(int Size)
        {
            Map = new ListElement<KeyValuePair<K, V>>[Size];
            this.Size = Size;
        }

        public void Put(KeyValuePair<K,V> v) 
        {
            var idx = hash(v.GetKey()) % Size;
            var next = new ListElement<KeyValuePair<K, V>>(v) { Next = null };
            if (Map[idx] == null)
            {
                Map[idx] = next;
            }
            else
            {
                // suche letztes objekt
                var obj = Map[idx];
                while (obj.Next != null)
                    obj = obj.Next;

                // add next
                obj.Next = next;
            }
        }

        public V Get(K Key)
        {
            var idx = hash(Key) % Size;
            if (Map[idx] == null)
                throw (new ArgumentException("Key nicht in Hashtabelle enthalten"));
            if (Map[idx].Next == null) // nur ein element an der Posititon
                return Map[idx].Value.GetValue();
            else // mehrere elemente an dieser position
            {
                var obj = Map[idx];
                while (obj != null)
                {
                    if (obj.Value.GetKey().Equals(Key))
                        return obj.Value.GetValue();
                    obj = obj.Next;
                }
                throw (new ArgumentException("Key nicht in Hashtabelle enthalten"));
            }
        }

        private int hash(K key)
        {
            var res = key.GetHashCode();
            return res < 0 ? -res : res;
        }

        private class ListElement<Vt>
        {
            public Vt Value { get; private set; }
            public ListElement<Vt> Next { get; set; }

            public ListElement(Vt v)
            {
                Value = v;
            }
        }
    }
}
