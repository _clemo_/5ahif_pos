﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS
{
    interface KeyValuePair<K, V>
    {
        K GetKey();
        V GetValue();
    }
}
