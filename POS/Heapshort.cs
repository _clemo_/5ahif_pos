﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS
{
    class Heapshort
    {
        private static void shiftDown<T>(ref T[] arr, int start, int end) where T : IComparable<T>
        {
            int root = start;
            while ((root << 1) + 1 <= end)
            {
                int child = (root << 1) + 1;
                int swap = root;
                if (arr[swap].CompareTo(arr[child]) < 0) // arr[swap] < arr[child]
                    swap = child;
                if (child + 1 <= end && arr[swap].CompareTo(arr[child + 1]) < 0) // arr[swap] < arr[child + 1]
                    swap = child + 1;
                if (swap != root)
                {
                    T temp = arr[swap];

                    arr[swap] = arr[root];
                    arr[root] = temp;

                    root = swap;
                }
                else
                    return;
            }
        }

        private static void heapyfy<T>(ref T[] arr, int count) where T : IComparable<T>
        {
            int start = (count - 2) >> 1;
            while (start >= 0)
            {
                shiftDown(ref arr, start, count - 1);
                --start;
            }
        }

        public static void heap<T>(T[] arr) where T : IComparable<T>
        {
            heapyfy(ref arr, arr.GetLength(0));
            int end = arr.GetLength(0) - 1;
            while (end > 0)
            {
                T temp = arr[0];
                arr[0] = arr[end];
                arr[end] = temp;

                --end;
                shiftDown(ref arr, 0, end);
            }
        }
    }
}
