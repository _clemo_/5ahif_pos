﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS
{
    class Program
    {
        static void Main(string[] args)
        {
            // teste heap-sort:
            var testData = new int[] { 6, 1, 7, 1, 3, 5, 6, 9, 0, 2 };
            Heapshort.heap(testData);
            Console.WriteLine(String.Join(" ",testData));
            Console.ReadLine();
        }
    }
}
