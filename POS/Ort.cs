﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS
{
    class Ort :KeyValuePair<string, Ort>
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Bezeichnung { get; set; }

        public string GetKey()
        {
            return Bezeichnung;
        }

        public Ort GetValue()
        {
            return this;
        }
    }
}
