﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Drucken
{
    public partial class Form1 : Form
    {
        PrintDocument pd = new PrintDocument();
        int i = 0;

        public Form1()
        {
            InitializeComponent();

            pd.BeginPrint += pd_BeginPrint;
            pd.PrintPage += pd_PrintPage;
            pd.QueryPageSettings += pd_QueryPageSettings;
        }



        void pd_QueryPageSettings(object sender, QueryPageSettingsEventArgs e)
        {
            
        }


        private Font arial = new Font("Arial", 10.0f);
        private Brush darkblue = new SolidBrush(Color.DarkBlue);
        private PointF start = new PointF(10f, 10f);
        private bool isSet = false;
        private int header = 0, footer = 0;
        private Margins margins = null;
        void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            if (i++ > 1337) return;
            e.HasMorePages = true;

            if (!isSet)
            {
                isSet = true;
                e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            }
            e.PageSettings.Margins = margins;

            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            e.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            e.Graphics.DrawString(i.ToString(), arial, darkblue,start );
        }

        void pd_BeginPrint(object sender, PrintEventArgs e)
        {
            i = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PrintPreviewDialog ppd = new PrintPreviewDialog();
            ppd.Document = pd;
            ppd.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Func<int, int> toMM =
                (inch) =>
                {
                    return (int)Math.Round((inch * 0.254f), 0);
                };

            Func<int, int> toIN =
                (mm) =>
                {
                    return (int)Math.Round((mm*(1/0.254f)), 0);
                };

            margins = pd.DefaultPageSettings.Margins;
            margins = new Margins(toMM(margins.Left), toMM(margins.Right), toMM(margins.Top), toMM(margins.Bottom));
            PrintSettings.GetSettings(ref margins, ref header, ref footer);
            pd.DefaultPageSettings.Margins = new Margins(toIN(margins.Left), toIN(margins.Right), toIN(margins.Top), toIN(margins.Bottom));
        }
    }
}
