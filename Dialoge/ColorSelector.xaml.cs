﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Dialoge
{
    public delegate void ColorChangedHandler(Color c);
    
    /// <summary>
    /// Interaktionslogik für ColorSelector.xaml
    /// </summary>
    public partial class ColorSelector : Window
    {
        public event ColorChangedHandler ColorChanged;

        private Color _startValue;
        private Color _ActualColor = Colors.White;
        public Color ActualColor
        {
            get { return _ActualColor; }
            set { 
                _ActualColor = value;
                sR.Value = _ActualColor.R;
                sG.Value = _ActualColor.G;
                sB.Value = _ActualColor.B;
            }
        }
        
        
        public ColorSelector(Color startValue)
        {
            InitializeComponent();
            ActualColor = startValue;
            _startValue = startValue;
        }

        private void sB_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if(sR == null || sG == null || sB == null) return;
            ActualColor = Color.FromRgb((byte)sR.Value, (byte)sG.Value, (byte)sB.Value);
            if (ColorChanged != null)
                ColorChanged(ActualColor);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (ColorChanged != null)
                ColorChanged(_startValue);
            Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
