﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KraVerTest
{
    public class Versicherungsmodell
    {
        public string Name { get; private set; } 
        // kann schnell durch "propg[tab][tab]" erzeugt werden. Kann von außen nur gelesen nicht geschrieben werden
        // da der Name wie der primärschlüssel ist, sollte er auf keinen Fall von außen geändert werden können.
        // In Java verwendet man hierfür Get und Ste-Methoden, C# erlaubt solceh einschränkungen auch direkt am Propoerty.

        public string Sozialversicherungsnummer { get; private set; }

        private static bool checkSovzVers(string soz) // prüft Sozialversicherungsnummer
        {
            int t;
            int sum = 0;
            foreach (char c in soz)
            {
                t = 0;
                if (Int32.TryParse("" + c, out t))
                    sum += t;
                else
                    return false;
            }

            return (sum % 9 == 0);
        }

        protected Versicherungsmodell() { }
        // Hinweis: man spricht zwar von einem "private-Konstruktor" defacto ist er aber 
        // protected (von außen private, aber inerhalb der Kalsse und der Sub-Klassen sichtbar

        protected static Versicherungsmodell GetVers(string Name, string svnr)
        {
            Versicherungsmodell v = new Versicherungsmodell() { Name = Name }; 
            // nach einem new [Klasse] kann man direkt innerhalb der {} public Variablen setzten.
            // hierbei muss man nicht wie sonst mit this.[variablenname] abgrenzen, da zuerst immer die Eigenschaft 
            // des neuen Objektes steht und es dadurch eindeutig wird (wie bei mir Name = Name)

            if(checkSovzVers(svnr)){
                v.Sozialversicherungsnummer = svnr;
                return v;
            }else
                return null;
        }

        public bool setSozVers(string soz)
        {
            /*
              // wenn in der Angabe keine nicht eine Methode hierfür gefordert wäre, wäre C#-Style so:
              private string _svnr = null;
              public string Sozialversicherungsnummer{
                get{
                    return _svnr;
                }
                set{
                    _svnr = checkSovzVers(soz) ? value : null;
                }             
              }
            */
            if (checkSovzVers(soz))
            {
                Sozialversicherungsnummer = soz;
                return true;
            }
            return false;
        }

        public string getInfo()
        {
            return this.ToString();
        }

        public override string ToString()
        {   
            // Tipp: immer in selbst geschriebenen Klassen ToString überschreiben, da der String bim Debugger angezeigt wird!
            return Name;
        }

        public virtual double getJahresPraemie() { return 0.0; } // hinweis: werden in subklassen überschrieben (durch virtual gekennzeichnet)
        public virtual double getVerguetung(int khTage) { return 0.0; } // hinweis: werden in sobklassen überschrieben (durch virtual gekennzeichnet)
    }

    public class VersicherungsmodellA : Versicherungsmodell
    {
        public int Jahrespraemie { get; private set; }

        private VersicherungsmodellA() {}
        // hier kann Konstruktor private sein, da von dieser Klasse nicht mehr weitervererbt wird 
        // (es wird meistens der Konstruktor der Super-Klasse benutzt um redundanzen etc zu vermeiden)

        public static VersicherungsmodellA GetVers(string Name, string svnr, int Jahrspraemie) // factory methode
        {
            // static = man benötigt keine objektinstanz um darauf zuzugreifen
            // public = außerhalb sichtbar
            // da Konstruktor von außen nicht sichtbar ist kann nur in eienr Methode innerhlab der Klasse der Konstruktor aufgerufen werden.
            // dabei ist es egal ob die Methode static ist oder nicht.
            // das heißt von außen kann kein objekt mehr erstellt werden, nur mithilfe der public static Methode können von außen neue Objekte erzeugt werden
            
            Versicherungsmodell v = Versicherungsmodell.GetVers(Name, svnr);
            if (v == null || Jahrspraemie <= 0) return null;

            VersicherungsmodellA va = v as VersicherungsmodellA;
            va.Jahrespraemie = Jahrspraemie;
            return va;
        }

        public override string ToString()
        {
            return base.ToString()+", Prämie/Jahr: "+Jahrespraemie;
        }

        public override double getJahresPraemie() // überschreibt super-Klasse (override)
        {
            return Jahrespraemie;
        }

        public override double getVerguetung(int khTage){ // überschreibt super-Klasse (override)
            return (Jahrespraemie/40.0)*khTage;
        }
    }

    public class VersicherungsmodellB : Versicherungsmodell
    {
        private VersicherungsmodellB() { }
        
        public double Grundpraemie { get; private set; }
        public double Monatspraemie { get; private set; }

        public static VersicherungsmodellB GetVers(string Name, string svnr, double Grundpraemie, double Monatspraemie)
        {
            Versicherungsmodell v = Versicherungsmodell.GetVers(Name, svnr);
            if (v == null || Grundpraemie <= 0 || Monatspraemie <= 0) return null;

            VersicherungsmodellB vb = v as VersicherungsmodellB;
            vb.Grundpraemie = Grundpraemie;
            vb.Monatspraemie = Monatspraemie;
            return vb;
        }

        public override double getJahresPraemie()
        {
            return (Monatspraemie * 12) + Grundpraemie;
        }

        public override double getVerguetung(int khTage)
        {
            double verg = 0;
            double jp = getJahresPraemie();

            if (khTage <= 0) return verg; // falls negative Werte übergeben wurden

            //ersten 10
            verg += (khTage < 10 ? khTage : 10) * (jp / 45.0);
            khTage -= 10;
            if (khTage <= 0) return verg;

            // 10-20
            verg += (khTage < 10 ? khTage : 10) * (jp / 40.0);
            khTage -= 10;
            if (khTage <= 0) return verg;

            //rest
            verg += khTage*(jp / 35.0);
            return verg;
        }
        
        public override string ToString()
        {
            return base.ToString() + ", Prämie/Jahr: " + getJahresPraemie();
        }
    }
}
