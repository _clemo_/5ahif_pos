﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KraVerTest
{
    class Kartei
    {
        private List<Versicherungsmodell> Versicherte = new List<Versicherungsmodell>();
        // um später unnötig die Liste durchsuchen zu müssen könnte man auch ein Dictionary<string, Versicherungsmodell> nehmen (Namen als Key).

        private int anzahl;

        // Konstruktor mit optionalem Parameter
        public Kartei(int maxVersicherte = 10)
        {
            anzahl = maxVersicherte;
        }

        /* // Alternative:
            public Kartei() : this(10) 
            { }
            public Kartei(int anzahl) { anzahl = maxVersicherte; }
          
            // Weitere Alternative:
               wie oben, nur mit redundatem Code
          
            // Anmerk:  in Java darf nur in der ersten Zeile eines Konstruktors ein anderer Konstruktor aufgerufen werden.
                        C# ist da strenger, heir muss man Konstruktoren vererben.
         */

        public bool addVers(string name, string svznr, int jp)
        {
            if (Versicherte.Count >= anzahl) return false;

            Versicherte.Add(VersicherungsmodellA.GetVers(name, svznr, jp));
            return true;
        }

        public bool addVers(string name, string svznr, double gp, double mp)
        {
            if (Versicherte.Count >= anzahl) return false;

            Versicherte.Add(VersicherungsmodellB.GetVers(name, svznr,gp,mp));
            return true;
        }

        public void printInfo()
        {
            Console.WriteLine(String.Join("\r\n", Versicherte)); // String.join schreibt angegeben String zwischen Listenelemente
        }

        private Versicherungsmodell GetVers(string name) // Sucht Versicherten mit Namen aus der Liste.
        {
            // Linq-Query (wie sql nur etwas vertauscht), performanter und leichter zu lesen als eine Schleife zum suchen
            var query = from Item in Versicherte
                        where Item != null && name.Equals(name)
                        select Item;

            int cnt = query.Count();
            if (cnt == 0)
            {
                throw (new ArgumentException("Name not in DB!"));
            }
            else if (cnt > 1)
            {
                throw (new ArgumentException("Name is multiple in DB!"));
            }
            Versicherungsmodell v = query.First(); // Linq ist wie SQL: ein Enumerable, da wir nur ein Element brauchen können wir mit First darauf zugreifen
            return v;
        }

        public double getVerguetung(string name, int khTage) // sollte int sein - finde double sinnvoller!
        {
            return GetVers(name).getVerguetung(khTage);
        }

        public bool setSozVersNr(string name, string s){ // fehler in angabe, sozvers als int?
            return GetVers(name).setSozVers(s);
        }

        public void reset()
        {
            Versicherte.Clear();
            // würde man anstelle der liste ein dictionary nutzen, muss das natürlich auch geleert werden
        }
    }
}
