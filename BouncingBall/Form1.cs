﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace BouncingBall
{
    public partial class Form1 : Form
    {
        Kugel k = null;
        Thread t = null;
        double[] direction = new double[]{1,1};
        bool moveBall = false;
        double mul = 5.0;
        System.Windows.Forms.Timer tim = new System.Windows.Forms.Timer();

        public Form1()
        {
            tim.Interval = 1;
            InitializeComponent();
            trackBar1.Value = (int)(mul * 100);
            k = new Kugel() { X = Width/2-25, Y = 150, Radius = 50 };
            this.Paint += Form1_Paint;
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            Random r = new Random();
            direction[0] = r.Next(100)/100f;
            direction[1] = r.Next(100)/100f;

            if (r.Next(2) == 1)
                direction[0] = -direction[0];
            if (r.Next(2) == 1)
                direction[1] = -direction[1];

            tim.Tick += tim_Tick;
        }

        void move()
        {
            Kugel.Bounce b = k.IsColliding(ClientSize);
            if (b != Kugel.Bounce.Nowhere)
                direction = bounceBack(direction, b);
            k.Move(new float[] { (float)(mul*direction[0]), (float)(mul*direction[1]) });
        }

        double[] bounceBack(double[] v, Kugel.Bounce b)
        {
            if (b == Kugel.Bounce.Nowhere) return v;
            else if (b == Kugel.Bounce.Top || b == Kugel.Bounce.Bottom) return new double[] { v[0], -v[1] };
            else return new double[] {-v[0],v[1]};
        }

        void Form1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            if(moveBall)
                move();
            k.Paint(e.Graphics, new SolidBrush(Color.Blue));
        }

        void tim_Tick(object sender, EventArgs e)
        {
            this.Invalidate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            moveBall = !moveBall;
            if (moveBall)
            {
                tim.Start();
            }
            else
                tim.Stop();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            moveBall = !moveBall;
            while (true)
            {
                this.Invalidate();
                this.Refresh();
                Thread.Sleep(1);
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            mul = trackBar1.Value / 100f;
        }
    }
}
