﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace BouncingBall
{
    public class Kugel
    {
        public enum Bounce {Top = 1, Left = 2, Right = 3, Bottom = 4, Nowhere = 0}
        public double X;
        public double Y;
        public double Radius;
        public RectangleF BoundingBox
        {
            get
            {
                Func<Double, int> round = (d) =>
                {
                    return (int)Math.Round(d, 0);
                };
                return new RectangleF((float)X, (float)Y, round(2 * Radius), round(2 * Radius));
            }
        }

        private static Object _lock = new Object();
        public void Paint(Graphics g, SolidBrush b, Pen p = null)
        {
            lock (_lock)
            {
                g.FillEllipse(b, BoundingBox);
                if (p != null)
                    g.DrawEllipse(p, BoundingBox);
            }
        }

        public Bounce IsColliding(Size s)
        {
            double r2 = Radius * 2;
            if (Y < 0) {
                Y = 0;
                return Bounce.Top; 
            }
            if (Y + r2 > s.Height) {
                Y = s.Height - r2;
                return Bounce.Bottom; 
            }
            if (X < 0) {
                X = 0;
                return Bounce.Left;
            }
            if (X + r2 > s.Width) {
                X = s.Width - r2;
                return Bounce.Right; 
            }
            return Bounce.Nowhere;
        }



        public void Move(float[] offs)
        {
            X += offs[0];
            Y += offs[1];
        }
    }
}
