﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collections
{
    public interface Stack<T>
    {
        void Push(T val);
        T Pop();
        T Peek();
    }
    
    public class StackList<T> : Stack<T>
    {
        OwnDoubleLinkedList<T> list = new OwnDoubleLinkedList<T>();

        public void Push(T val)
        {
            list.AppendAtEnd(val);
        }

        public T Pop()
        {
            var tmp = list.GetLast();
            list.RemoveLast();
            return tmp;
        }

        public T Peek()
        {
            return list.GetLast();
        }
    }

    public class StackArray<T> : Stack<T>
    {
        protected int maxAmount = 0;
        protected int idx = -1;
        protected T[] Data;

        public StackArray(int Size)
        {
            maxAmount = Size;
            idx = -1;
            Data = new T[maxAmount];
        }

        public void Push(T val)
        {
            if (idx +1 >= maxAmount) throw (new StackOverflowException());
            Data[++idx] = Utility.CopyIfPossible(val);
        }

        public T Pop()
        {
            var ret = Peek();
            idx--;
            return ret;
        }

        public T Peek()
        {
            if (idx < 0) throw (new InvalidOperationException("Stack is empty!"));
            return Utility.CopyIfPossible(Data[idx]);
        }
    }
}
