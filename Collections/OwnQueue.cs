﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collections
{
    public interface Queue<T>
    {
        void Put(T val);
        T Get();
    }
    
    public class QueueList<T> : Queue<T>
    {
        protected OwnDoubleLinkedList<T> list = new OwnDoubleLinkedList<T>();
        public void Put(T val)
        {
            list.AppendAtEnd(val);
        }

        public T Get()
        {
            var ret = list.GetFirst();
            list.RemoveFirst();
            return ret;
        }
    }

    public class QueueArray<T> : Queue<T> // unschöne Lösung, aber grenz Ringbuffer gut von Queue ab
    {
        T[] Data;
        int size;
        int inUse = 0;

        public QueueArray(int Size)
        {
            size = Size;
            Data = new T[size];
        }

        public void Put(T val)
        {
            if (inUse >= size)
                throw (new NotSupportedException("Exceeded queue maximum!"));
            Data[inUse++] = val;
        }

        public T Get()
        {
            if (inUse == 0)
                throw (new ArgumentException("Queue is empty!"));
            var d = Data[0];
            for (int i = 1; i < inUse; ++i)
                Data[i - 1] = Data[i];
            --inUse;
            return d;
        }
    }
}
