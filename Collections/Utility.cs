﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collections
{
    public class Utility
    {
        public static T CopyIfPossible<T>(T value)
        {
            return (value is ICloneable) ? (T)(value as ICloneable).Clone() : value;
        }

        public static int AbsDiff(int a, int b)
        {
            return a > b ? a - b : b - a;
        }

        public static int StringToHash<T>(T input)
        {
            // holt sich bytes vom string, und interpretiert diese folge als eine zahl zur basis 255 (dezimal: basis 10, binär: basis 2)
            
            var s = input.ToString();

            byte[] data = Encoding.ASCII.GetBytes(s.ToCharArray());
            UInt64 res = 0; // kann sehr groß werden!
            // uint64: integer mit 64 bit länge ohne vorzeichen bit. d.h. uint64 ist doppelt so groß wie int64
            foreach (var b in data)
            {
                res *= 255;
                res += b;
            }

            // da uinit64 zu groß > machen wir int daraus
            res = res % Convert.ToUInt64(Int32.MaxValue);
            return Convert.ToInt32(res);
        }
    }
}
