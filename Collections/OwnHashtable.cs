﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collections
{
    public abstract class Hashtable<KeyT, ValueT>
    {
        public virtual void Put(KeyT key, ValueT val) { }
        public virtual ValueT Get(KeyT key){ return default(ValueT);}

        protected class KeyValuePair
        {
            public KeyT Key { get; set; }
            public ValueT Value { get; set; }
        }
    }

    /* Erklärung zu Func
     * 
     * Bsp: Funktion ohne Parameter die einen Integer zurückgibt
     * Func<int> intFunc = () => {return 3;};
     * Console.WriteLine(intFunc());
     * 
     * Bsp: Funtkion mit Parameter die einen Integer zurrückgibt
     * Func<int,int> sqare = (i) => {return i * i;};
     * Bei i muss kein Typ anagegeben werden, da dieser schon im Func angegeben wurde!
     * 
     * Hinweis: der Letzte typ ist immer der Rückgabe Typ!
     * 
     * Bsp: Func die Zahlen ausgit
     * Func<int, string> = (z) => {
     *      switch(z){
     *          case 0:
     *              return "null";
     *          case 1:
     *              return "eins";
     *          case 2:
     *              return "zwei";
     *          case 3:
     *              return "drei":
     *         [....]
     *          defaukt:
     *              return null;
     *      }
     * }
     */


    public class HashtableExternVerkettet<KeyType, ValueType> : Hashtable<KeyType, ValueType>
    {
        private OwnDoubleLinkedList<KeyValuePair>[] Data; 
        // im feld data wird abgespeichert, wenn kollission, dann wird in liste eingefügt
        int capacity;
        Func<KeyType, int> HashFunction;

        public HashtableExternVerkettet(int Capacity, Func<KeyType, int> userDefinedHashFunction = null)
        {
            capacity = Capacity;
            Data = new OwnDoubleLinkedList<KeyValuePair>[capacity];
            for (int i = 0; i < capacity; i++)
            {
                Data[i] = new OwnDoubleLinkedList<KeyValuePair>();
            }

            // statt per func könnte man auch "hard" in die klasse eincodieren als funktion
            if (userDefinedHashFunction != null)
            {
                HashFunction = userDefinedHashFunction;
            }
            else
            {
                HashFunction = (key) => { int ret = key.GetHashCode(); if (ret < 0) ret = -ret; return ret; }; // default: Object.GetHashCode()
                // sollte Object.GetHashCode nicht erlaub sein Utility.StringToHash nutzen!
                // getHashcode kanna uch negative werte liefern!
            }
        }

        public override void Put(KeyType key, ValueType val)
        {
            var newData = new KeyValuePair() { Key = key, Value = val };
            int hash = HashFunction(key)%capacity;

            if (Data[hash].Count() == 0) // noch leer: keine Probleme
            {
                Data[hash].AppendAtEnd(newData);
            }
            else // schon was drinnen: prüfe ob key schon in verwendung
            {
                bool inUse = false;
                foreach (var item in Data[hash]) // iteration durch liste
                {
                    if (item.Key.Equals(key))
                    {
                        inUse = true;
                        break; // ist unsauber aber einfach!
                    }
                }

                if (inUse)
                    throw (new ArgumentException("key is already in use!"));

                Data[hash].AppendAtEnd(newData);
            }
        }

        public override ValueType Get(KeyType key)
        {
            int hash = HashFunction(key) % capacity;
            if (Data[hash].Count() == 0)
            {
                throw (new ArgumentException("Key unknown!"));
            }
            else if (Data[hash].Count() == 1) // nur ein Objekt vorhanden: kein Problem
            {
                return Data[hash].GetFirst().Value;
            }
            else // suche key
            {
                foreach (var item in Data[hash])
                {
                    if (item.Key.Equals(key))
                        return item.Value;
                }
                throw (new ArgumentException("Key unknown!"));
            }
        }
    }

    public class HashtableInternVerkettet<KeyType, ValueType> : Hashtable<KeyType, ValueType>
    {
        Func<KeyType, int> InternalHashFunction;
        Func<int, int> ExternalHashmanipulatingFunction;
        int capacity;
        int fillState = 0;
        int? lastSalt = null;
        KeyValuePair[] Data;

        public HashtableInternVerkettet(int Capacity, 
            Func<KeyType, int> userDefinedInternalHashFunction = null, 
            Func<int, int> userDefinedHashManipulation  = null)
        {
            capacity = Capacity;
            Data = new KeyValuePair[capacity];
            for (int i = 0; i < capacity; i++)
            {
                Data[i] = null;
            }

            // auch hier: es ist genauso möglich die hash-funktionen als Funktionen zurealisieren anstelle von Func's
            if (userDefinedInternalHashFunction != null)
            {
                InternalHashFunction = userDefinedInternalHashFunction;
            }
            else
            {
                InternalHashFunction = (key) => { int ret = key.GetHashCode(); if (ret < 0) ret = -ret; return ret; }; // default: Object.GetHashCode()
                // sollte Object.GetHashCode nicht erlaub sein Utility.StringToHash nutzen!
                // getHashcode kanna uch negative werte liefern!
            }

            if (userDefinedHashManipulation != null)
            {
                ExternalHashmanipulatingFunction = userDefinedHashManipulation;
            }
            else
            {
                ExternalHashmanipulatingFunction = (oldHash) => {
                    // suche kleinste ungerade zahl welche die kapazität nur mit rest teilt (um zu vermeiden in schleifen zu laufen)
                    if (lastSalt == null)
                    {
                        int salt = 3;
                        while (salt % capacity == 0)
                            salt += 2;
                        lastSalt = salt;
                    }
                    return oldHash + (int)lastSalt;
                };
            }
        }

        public override void Put(KeyType key, ValueType val)
        {
            if (fillState + 1 >= capacity)
                throw (new ArgumentOutOfRangeException("Hashtable is full!"));

            var newData = new KeyValuePair() { Key = Utility.CopyIfPossible(key), Value = Utility.CopyIfPossible(val) };
            int hash = InternalHashFunction(key);
            if (hash >= capacity)
                hash = hash % capacity;

            if (Data[hash] == null) // wenn frei
            {
                Data[hash] = newData;
                ++fillState;
            }
            else // suche neuen Platz
            {
                int badIdx = hash; // um zu erkennen ob "im kreis" gegangen wird

                do
                {
                    if(Data[hash].Key.Equals(key))
                        throw(new ArgumentException("key is already in use!"));
                    hash = ExternalHashmanipulatingFunction(hash);
                    if (hash >= capacity)
                        hash = hash % capacity;
                } while (Data[hash] != null && hash != badIdx);

                if (hash == badIdx)
                    throw (new NotSupportedException("can't store this key!"));

                Data[hash] = newData;
                ++fillState;
            }
        }

        public override ValueType Get(KeyType key)
        {
            int hash = InternalHashFunction(key);
            if (hash >= capacity)
                hash = hash % capacity;
            if (Data[hash] == null)
                throw (new ArgumentException("Key unknown!"));
            if (Data[hash].Key.Equals(key))
                return Utility.CopyIfPossible(Data[hash].Value);
            else
            {
                int badIdx = hash;
                do
                {
                    if (Data[hash].Key.Equals(key))
                        return Utility.CopyIfPossible(Data[hash].Value);
                    hash = ExternalHashmanipulatingFunction(hash);
                    if (hash >= capacity)
                        hash = hash % capacity;
                } while (hash != badIdx);

                throw (new NotSupportedException("can't find the value!"));
            }
        }
    }
}
