﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collections
{
    public class OwnDoubleLinkedList<T>
    {
        protected OwnListItem First = null;
        protected OwnListItem Last = null;
        private int count = 0;
        private bool isRingBuffer = false;


        private OwnListItem current = null;
        private int currentIdx = -1;

        public OwnDoubleLinkedList(bool ringReferences = false)
        {
            isRingBuffer = ringReferences;
        }

        private OwnListItem GetListItem(int idx)
        {
            var Node = First;
            int i = 0;
            while (Node != null && i < idx)
            {
                ++i;
                Node = Node.Next;
            }

            if (i < idx)
                return null;
            return Node;
        }

        protected OwnListItem CreateListItem(T value)
        {
            var item = new OwnListItem(value);

            if (First == null)
                First = item;
            if (Last == null)
                Last = item;
            ++count;
            return item;
        }

        public int Count()
        {
            return count;
        }

        public T GetFirst()
        {
            if (First == null)
                throw (new ArgumentException());
            return Utility.CopyIfPossible(First.DATA);
        }

        public T GetLast()
        {
            if (Last == null)
                throw (new ArgumentException());
            return Utility.CopyIfPossible(Last.DATA);
        }

        #region optional: GetAtIdx
        public T GetAtIdx(int idx)
        {
            // da index-Zugriff bei verketteten Listen lange dauert wird hier etwas optimiert

            if (isRingBuffer)
            {
                while (idx >= count)
                    idx -= count;
            }

            if (idx == 0) // wenn idx = 0, brauchen wir nciht suchen
                return GetFirst();
            else if (idx == count - 1) // wenn idx = max, bracuehen wir nicht suchen
                return GetLast();
            else if (idx < 0 || idx >= count) // wenn kein valider idx angegeben sollten wir nicht suchen
                throw (new ArgumentOutOfRangeException());
            else if (First == null || Last == null) // prüfe ob zugriff überhaupt möglich
                throw (new NotSupportedException());
            else if (idx == currentIdx) // wenn positionszeiger schon auf position ist, brauchen wir nicht suchen
                return Utility.CopyIfPossible(current.DATA);
            else
            {
                if (current == null || currentIdx == -1) // wenn positionszeiger noch nicht initialisiert wurde
                {
                    // prüfe ob idx näher am ende oder näher am anfang ist und setze positionszeiger
                    int a = Utility.AbsDiff(0, idx);
                    int b = Utility.AbsDiff(count - 1, idx);

                    if (a < b)
                    {
                        current = First;
                        currentIdx = 0;
                    }
                    else
                    {
                        current = Last;
                        currentIdx = count - 1;
                    }
                }
                else // wenn es positionszeiger schon gipt, setzte positionszeiger wenn nötig auf start oder ende um
                {
                    int diffToFirst = Utility.AbsDiff(0, idx);
                    int diffToLast = Utility.AbsDiff(count - 1, idx);
                    int diffToPos = (current == First || current == Last) ? 0 : Utility.AbsDiff(currentIdx, idx);

                    if (diffToFirst < diffToLast && diffToFirst < diffToPos)
                    {
                        current = First;
                        currentIdx = 0;
                    }
                    else if (diffToLast < diffToFirst && diffToLast < diffToPos)
                    {
                        current = Last;
                        currentIdx = count - 1;
                    }
                }

                bool useNext = idx > currentIdx; // prüfe in welche richting iteriert werden muss
                while (current != null && currentIdx != idx)
                {
                    if (useNext)
                    {
                        current = current.Next;
                        ++currentIdx;
                    }
                    else
                    {
                        current = current.Pref;
                        --currentIdx;
                    }
                }

                if (currentIdx != idx)
                {
                    return default(T);
                }
                else
                {
                    return Utility.CopyIfPossible(current.DATA);
                }
            }
        }
        #endregion

        public void AppendAtEnd(T value)
        {
            var item = CreateListItem(Utility.CopyIfPossible(value));

            
            item.Pref = Last;
            Last.Next = item;

            if (isRingBuffer)
            {
                First.Pref = item;
                item.Next = First;
            }
            Last = item;
        }

        public void AppendAtBegin(T value)
        {
            var item = CreateListItem(Utility.CopyIfPossible(value));

            item.Next = First;
            First.Pref = item;
            if (isRingBuffer)
            {
                Last.Next = item;
                item.Pref = Last;
            }
            First = item;
        }

        public void RemoveFirst()
        {
            if (First == null)
                throw (new ArgumentException());

            --count;

            if (First == First.Next)
                First = null;
            else
                First = First.Next;

            if (First == null)
                Last = null;
            else
                First.Pref = null;
        }
        
        public void RemoveLast()
        {
            if (Last == null)
                throw (new ArgumentException());

            --count;

            Last = Last.Pref;
            if (Last == null)
                First = null;
            else
                Last.Next = null;
        }

        public void RemoveAt(int idx)
        {
            var itm = GetListItem(idx);

            if (itm == null)
                throw (new ArgumentException());

            --count;

            var pref = itm.Pref;
            var next = itm.Next;

            pref.Next = next;
            next.Pref = pref;
        }

        // iterator: grundstruktur kann man mit iterator{tab}{tab} erzeugen
        public System.Collections.Generic.IEnumerator<T> GetEnumerator()
        {
            var Node = First;
            while(Node != null){
                yield return Utility.CopyIfPossible(Node.DATA);
                Node = Node.Next;
            }
        }

        public override string ToString() // nur zum debuggen!!!
        {
            List<string> elements = new List<string>();
            var Node = First;
            if(count > 0)
                while (true)
                {
                    elements.Add(Node.DATA.ToString());
                    Node = Node.Next;
                    if (Node == First)
                        break;
                }
            return "{"+String.Join(", ",elements.ToArray())+"}";
        }

        protected class OwnListItem
        {
            public T DATA;
            public OwnListItem Next = null;
            public OwnListItem Pref = null;

            public OwnListItem(T Data)
            {
                DATA = Data;
            }
        }
    }
}
