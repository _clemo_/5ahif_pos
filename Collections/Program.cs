﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            var arrStack = new StackArray<int>(10);
            var lstStack = new StackList<int>();
            int[] testdata = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };
            Console.WriteLine("arrayStack: "+TestStack(arrStack,testdata, 10));
            Console.WriteLine("listStack: "+TestStack(lstStack, testdata));

            var arrQueue = new QueueArray<int>(10);
            var lstQueue = new QueueList<int>();
            Console.WriteLine("arrayQueue: "+TestQueue(arrQueue, testdata, 10));
            Console.WriteLine("listQueue: "+TestQueue(lstQueue, testdata));


            var arrRing = new RingbufferArray<int>(10);
            var lstRing = new RingbufferList<int>(10);
            Console.WriteLine("arrayRingBuffer: "+TestRingbuffer(arrRing, testdata, 10));
            Console.WriteLine("listRingBuffer: "+TestRingbuffer(lstRing, testdata, 10));

            // testdaten: Mo-Do - ein zwei polizei
            var keys = new string[]     {"eins zwei","drei vier", "fünf sechs","sieben acht", "Eins Zwei", "Drei Vier", "Fünf Sechs", "Sieben Acht", "Ja Ja Ja",    "ja ja ja"};
            var values = new string[]   {"polizei",  "grenadier", "alte hex",  "gute nacht",  "Polizei",   "Grenadier", "Alte Hex'",  "Gute Nacht",  "Was is los?", "Was ist das?"};
            // anzahl daten: 10. daten sollten immer 80% der capazität sein; kazaität > 12,5 = 13

            var hashExt = new HashtableExternVerkettet<string, string>(13);
            var hashInt = new HashtableInternVerkettet<string, string>(13);

            Console.WriteLine("hashIntern: " + TestHashTable(hashInt,keys, values));
            Console.WriteLine("hashExtern: " + TestHashTable(hashExt, keys, values));
            Console.ReadLine();
        }

        static bool TestStack<T>(Stack<T> s, T[] data, int? max = null)
        {
            try
            {
                s.Peek();
                return false;
            }
            catch
            {

            }

            try
            {
                s.Pop();
                return false;
            }
            catch
            {

            }

            int dLen = data.GetLength(0);
            if(max != null){
                int m = (int)max;
                if(m < dLen)
                    dLen = m;
            }

            for (int i = 0; i < dLen; ++i)
            {
                try
                {
                    s.Push(data[i]);
                }
                catch
                {
                    return false;
                }
            }

            if (max != null)
                try
                {
                    s.Push(data[0]);
                    return false;
                }
                catch
                {

                }

            for (int i = dLen-1; i >= 0; --i)
            {
                try
                {
                    if (!s.Pop().Equals(data[i]))
                        return false;
                }
                catch
                {
                    return false;
                }
            }


            for (int i = 0; i < data.GetLength(0); i++)
            {
                s.Push(data[i]);
                if (!s.Pop().Equals(data[i]))
                    return false;
            }

            return true;
        }

        static bool TestQueue<T>(Collections.Queue<T> q, T[] data, int? max = null)
        {
            try
            {
                q.Get();
                return false;
            }
            catch
            {

            }

            int dLen = data.GetLength(0);
            if (max != null)
            {
                int m = (int)max;
                if (m < dLen)
                    dLen = m;
            }
            for (int i = 0; i < dLen; ++i)
                q.Put(data[i]);

            if (max != null)
                try
                {
                    q.Put(data[0]);
                    return false;
                }
                catch
                {

                }

            for (int i = 0; i < dLen; ++i)
                if (!q.Get().Equals(data[i]))
                    return false;

            for (int i = 0; i < data.GetLength(0); i++)
            {
                q.Put(data[i]);
                if (!q.Get().Equals(data[i]))
                    return false;
            }
            return true;
        }

        static bool TestRingbuffer<T>(Ringbuffer<T> r, T[] data, int max)
        {
            try
            {
                r.Read();
                return false;
            }
            catch { }

            for (int i = 0; i < max; i++)
            {
                r.Write(data[i]);
            }

            for (int i = 0; i < max; i++)
            {
                if (!r.Read().Equals(data[i]))
                    return false;
            }

            for (int i = 0; i < data.GetLength(0); i++)
            {
                r.Write(data[i]);
                if (!r.Read().Equals(data[i]))
                    return false;
            }

            return true;
        }

        static bool TestHashTable<K, V>(Hashtable<K, V> t, K[] keys, V[] data)
        {
            int maxK = keys.GetLength(0);
            int maxY = data.GetLength(0);
            int max = maxK < maxY ? maxK : maxY;

            try
            {
                t.Get(keys[0]);
                return false;
            }
            catch
            {

            }

            try
            {
                for (int i = 0; i < max; i++)
                {
                    t.Put(keys[i], data[i]);
                    if (!t.Get(keys[i]).Equals(data[i]))
                        return false;
                }

                for (int i = 0; i < max; i++)
                {
                    if (!t.Get(keys[i]).Equals(data[i]))
                        return false;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
