﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Collections
{
    interface Ringbuffer<T>
    {
        void Write(T value);
        T Read();
    }

    // Ringbuffer = Queue mit maxLength
    
    class RingbufferList<T> : Ringbuffer<T> 
    {
        OwnDoubleLinkedList<T> list = new OwnDoubleLinkedList<T>();
        int Size;

        public RingbufferList(int Size)
        {
            this.Size = Size;
        }

        public void Write(T value)
        {
            if (list.Count() >= Size) throw (new IndexOutOfRangeException("Not enough space in Buffer!"));
            list.AppendAtEnd(value);
        }

        public T Read()
        {
            if (list.Count() == 0) throw (new NotSupportedException("Buffer is Empty!"));
            var ret = list.GetFirst();
            list.RemoveFirst();
            return ret;
        }
    }

    class RingbufferArray<T> : Ringbuffer<T>
    {
        protected int idxWrite;
        protected int idxRead;
        protected int maxIdx;
        protected T[] Data;
        int count = 0;

        public RingbufferArray(int Size)
        {
            idxWrite = 0;
            idxRead = -1;
            maxIdx = Size;
            Data = new T[maxIdx];
        }

        public void Write(T val)
        {
            if (idxWrite == idxRead)
                throw (new InvalidOperationException("Not enought space in queue available!"));
            Data[idxWrite] = Utility.CopyIfPossible(val);
            int idx = idxWrite + 1;
            if (idx >= maxIdx) idx -= maxIdx;
            idxWrite = idx;
            count++;
        }

        public T Read()
        {
            if (count == 0) throw (new NotSupportedException("Ringbuffer is empty!"));
            int idx = idxRead + 1;
            if (idx >= maxIdx) idx -= maxIdx;
            --count;
            return Utility.CopyIfPossible(Data[idxRead = idx]);
        }
    }
}
