﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BouncingBall;

namespace BouncingBalls
{
    public partial class Form1 : Form
    {
        List<Kugel> Kugeln = new List<Kugel>();
        List<float[]> directions = new List<float[]>();
        private Rectangle Bet = new Rectangle();
        private int BetWidth = 5;
        private int BetHeight = 30;
        System.Windows.Forms.Timer tim = new System.Windows.Forms.Timer();
        private float mul = 1.5f;
        private Random r = new Random();

        public Form1()
        {
            InitializeComponent();
            Bet.Y = ClientSize.Height / 2 - BetHeight / 2;
            Bet.Width = BetWidth;
            Bet.Height = BetHeight;
            AdjustBet();
            tim.Interval = 33;

            generateBall();
            generateBall();
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            this.Resize += Form1_Resize;
            this.MouseMove += Form1_MouseMove;
            this.Paint += Form1_Paint;
            tim.Tick += tim_Tick;
            tim.Start();
        }

        void tim_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }

        void Form1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            e.Graphics.FillRectangle(Brushes.Blue, Bet);
            e.Graphics.DrawRectangle(Pens.DarkBlue, Bet);
            var p = new Pen(Color.DarkOrange, 2f);
            var b = new SolidBrush(Color.Orange);
            int m = Kugeln.Count;
            if (m > 2000)
            {
                int fp = m / 4;
                int sp = m / 2;
                int tp = m / 3;
                Parallel.Invoke(
                    () =>
                    {
                        for (int i = 0; i < fp; i++)
                        {
                            directions[i] = move(Kugeln[i], directions[i]);
                            Kugeln[i].Paint(e.Graphics, b, p);
                        }
                    },
                    () =>
                    {
                        for (int i = fp; i < sp; i++)
                        {
                            directions[i] = move(Kugeln[i], directions[i]);
                            Kugeln[i].Paint(e.Graphics, b, p);
                        }
                    },
                    () =>
                    {
                        for (int i = sp; i < tp; i++)
                        {
                            directions[i] = move(Kugeln[i], directions[i]);
                            Kugeln[i].Paint(e.Graphics, b, p);
                        }
                    },
                    () =>
                    {
                        for (int i = sp; i < m; i++)
                        {
                            directions[i] = move(Kugeln[i], directions[i]);
                            Kugeln[i].Paint(e.Graphics, b, p);
                        }
                    }
                );
            }
            else if (m > 1000)
            {
                int fh = m / 2;
                Parallel.Invoke(
                    () =>
                    {
                        for (int i = 0; i < fh; i++)
                        {
                            directions[i] = move(Kugeln[i], directions[i]);
                            Kugeln[i].Paint(e.Graphics, b, p);
                        }
                    },
                    () =>
                    {
                        for (int i = fh; i < m; i++)
                        {
                            directions[i] = move(Kugeln[i], directions[i]);
                            Kugeln[i].Paint(e.Graphics, b, p);
                        }
                    }
                );
            }
            else
            {
                for (int i = 0; i < m; ++i)
                {
                    directions[i] = move(Kugeln[i], directions[i]);
                    Kugeln[i].Paint(e.Graphics, b, p);
                }
            }
            this.Text = "Bouncing " + m + " Balls";
        }

        void generateBall()
        {
            Kugeln.Add(new Kugel() { X = r.Next(ClientSize.Width), Y = r.Next(ClientSize.Height), Radius = 15f });
            directions.Add(new float[] { r.Next(3) - 1, 0 });
            while (directions.Last()[0] == 0)
            {
                directions.Last()[0] = r.Next(3) - 1;
            }
        }

        private bool between(double val, double min, double max)
        {
            return val >= min && val <= max;
        }

        private bool overlaps(Rectangle a, Rectangle b)
        {
            if (between(b.X, a.X, a.X + a.Width))
                return true;
            if (between(a.X, b.X, b.X + b.Width))
                return true;
            return false;
        }

        float[] move(Kugel k, float[] direction)
        {
            bool bounced = false;
            Kugel.Bounce b = k.IsColliding(ClientSize);
            double r = k.Radius;
            double r2 = 2 * k.Radius;
            
            if (b != Kugel.Bounce.Nowhere)
                direction = bounceBack(direction, b);
            else if (k.Y + r2 >= Bet.Y && k.Y <= Bet.Y + BetHeight)
            {
                var rec = new Rectangle((int)k.X, (int)k.Y, (int)r2, (int)r2);
                if (overlaps(Bet, rec))
                {
                    double xr = k.X + r;
                    if (xr < Bet.X + BetWidth / 2)
                    {
                        b = Kugel.Bounce.Left;
                        bounced = true;
                    }
                    else
                    {
                        b = Kugel.Bounce.Right;
                        bounced = true;
                    }

                    if (b != Kugel.Bounce.Nowhere)
                    {
                        direction = bounceBack(direction, b);
                        ThreadPool.QueueUserWorkItem(
                            (o) => {
                                var f = new float[] { (float)(mul * direction[0]), (float)(mul * direction[1]) };
                                while (overlaps(Bet, new Rectangle((int)k.X, (int)k.Y, (int)r2, (int)r2)))
                                {
                                    k.Move(f);
                                }
                            }
                        );
                    }
                }
            }
            k.Move(new float[] { (float)(mul * direction[0]), (float)(mul * direction[1]) });

            if (bounced)
            {
                generateBall();
            }

            return direction;
        }

        float[] bounceBack(float[] v, Kugel.Bounce b)
        {
            if (b == Kugel.Bounce.Nowhere) return v;
            else if (b == Kugel.Bounce.Top || b == Kugel.Bounce.Bottom) return new float[] { v[0], -v[1] };
            else return new float[] { -v[0], v[1] };
        }

        void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            Bet.Y = e.Y - BetHeight / 2;
        }

        void Form1_Resize(object sender, EventArgs e)
        {
            AdjustBet();
        }

        private void AdjustBet()
        {
            Bet.X = ClientSize.Width / 2 - BetWidth / 2;

            if (Bet.Y + BetHeight / 2 >= ClientSize.Height)
                Bet.Y = ClientSize.Height - BetHeight / 2;
        }
    }
}
