﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Uhr
{
    public delegate DateTime GetDateTime();
    class Uhr
    {
        public event GetDateTime getTimeHandler;
        public static Pen LongLine = new Pen(Color.DarkBlue, 4f);
        public static Pen ShortLine = new Pen(Color.Black, 3f);
        public static float longLineLength = 20;
        public static float shortLineLenth = 10;
        public static Pen StundenZeiger = new Pen(Color.Black, 4f);
        public static Pen MinutenZeiger = new Pen(Color.DarkGreen, 3f);
        public static Pen SekundenZeiger = new Pen(Color.DarkRed, 2f);

        private static double toRad(double degree)
        {
            return (Math.PI * degree) / 180;
        }

        private static int round(double d)
        {
            return (int)Math.Round(d, 0);
        }

        private static Point calcAngle(double angle, float radius, Point center)
        {
            var rad = toRad(angle);
            return new Point(
                round(radius * Math.Cos(rad) + center.X),
                round(radius * Math.Sin(rad) + center.Y));
        }

        private static Point getInnerPoint(Point p, float d, float radius, Point center)
        {
            var offsX = round((((p.X - center.X) / radius) * d));
            var offsY = round((((p.Y - center.Y) / radius) * d));

            return new Point(p.X - offsX, p.Y - offsY);
        }

        public void PaintZiffernblatt(Graphics g, Point Center, float radius)
        {
            var rect = new Rectangle(
                (int)(Center.X - radius),
                (int)(Center.Y - radius),
                (int)(2 * radius),
                (int)(2 * radius));
            g.DrawEllipse(Pens.Black, rect);

            List<Point> longLinesStart = new List<Point>()
            {
                new Point(Center.X,round(Center.Y-radius)),
                new Point(round(Center.X+radius),Center.Y),
                new Point(Center.X,round(Center.Y+radius)),
                new Point(round(Center.X-radius),Center.Y)
            };

            float i = 360 / 12f;

            List<Point> shortLinesStart = new List<Point>()
            {
                calcAngle(i,radius,Center),
                calcAngle(2*i,radius,Center),
                calcAngle(4*i,radius,Center),
                calcAngle(5*i,radius,Center),
                calcAngle(7*i,radius,Center),
                calcAngle(8*i,radius,Center),
                calcAngle(10*i,radius,Center),
                calcAngle(11*i,radius,Center)
            };

            foreach (var item in longLinesStart)
            {
                var inner = getInnerPoint(item, longLineLength, radius, Center);
                g.DrawLine(LongLine, item, inner);
            }
            foreach (var item in shortLinesStart)
            {
                var inner = getInnerPoint(item, shortLineLenth, radius, Center);
                g.DrawLine(ShortLine, item, inner);
            }
        }

        public void PaintStundenZeiger(Graphics g, Point center, float radius)
        {
            var t = Time();
            var hrs = (t.Hour > 12 ? t.Hour - 12 : t.Hour) / 12f;
            var angle = hrs * 360 - 90;
            var p = calcAngle(angle, radius, center);
            g.DrawLine(StundenZeiger, center, p);
        }
        public void PaintMinutenZeiger(Graphics g, Point center, float radius)
        {
            var t = Time();
            var mins = t.Minute / 60f;
            var angle = (360 * mins) - 90;
            var p = calcAngle(angle, radius, center);
            g.DrawLine(MinutenZeiger, center, p);
        }
        public void PaintSekundenZeiger(Graphics g, Point center, float radius)
        {
            var t = Time();
            double secs = t.Second / 60f;
            var angle = (360 * secs) - 90;
            var p = calcAngle(angle, radius, center);
            g.DrawLine(SekundenZeiger, center, p);
        }

        private DateTime Time()
        {
            return getTimeHandler != null ? getTimeHandler() : DateTime.Now;
        }
    }
}
