﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Uhr
{
    public partial class Form1 : Form
    {
        DateTime? lastDraw = null;

        Uhr u = new Uhr();

        public Form1()
        {
            InitializeComponent();

            // sonst falckert es
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            // verarbeite events
            this.Paint += Form1_Paint;
            this.SizeChanged += Form1_SizeChanged;

            //timer um sekündlich neu zu zeichnen
            Timer tim = new Timer();
            tim.Interval = 1000;
            tim.Tick += sekunden_Tick;
            tim.Start();
        }

        void sekunden_Tick(object sender, EventArgs e)
        {
            // stelle fest ob sich "zeit" geändert hat
            bool draw = false;
            if (lastDraw == null) 
            {
                draw = true;
                lastDraw = DateTime.Now;
            }
            else
            {
                DateTime ld = (DateTime)lastDraw;
                DateTime n = DateTime.Now;
                draw = ld.Hour != n.Hour || ld.Minute != n.Minute || ld.Second != n.Second;
                lastDraw = n;
            }
            if(draw)
                Invalidate(); // zeichne ui neu
        }

        void Form1_SizeChanged(object sender, EventArgs e)
        {
            Invalidate();
        }

        void Form1_Paint(object sender, PaintEventArgs e)
        {
            int w = (int)e.Graphics.ClipBounds.Width;
            int h = (int)e.Graphics.ClipBounds.Height;
            var center = new Point(w / 2, h / 2);
            var rad = (w < h ? w : h) * 0.4f;

            Uhr.longLineLength = rad * 0.20f;
            Uhr.shortLineLenth = rad * 0.10f;

            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            u.PaintZiffernblatt(e.Graphics, center, rad);
            u.PaintStundenZeiger(e.Graphics, center, rad*0.8f);
            u.PaintMinutenZeiger(e.Graphics, center, rad*0.9f);
            u.PaintSekundenZeiger(e.Graphics, center, rad);
            
        }
    }
}
