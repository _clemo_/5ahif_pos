﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MöglichePaBsp
{
    class Singelton
    {
        private static Singelton instanz = null; // instanzvariable
        
        private Singelton (){} // privater kosntruktor

        public static Singelton GetInstanz() // funktion zum abfragen der instanz
        {
            if(instanz == null)
                instanz = new Singelton(); // aufruf nur innerhalb der klasse möglich
            return instanz;
        }
    }
}
