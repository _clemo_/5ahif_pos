﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace A_Stern_Simpel
{
    class Strasse
    {
        public string Von { get; private set; }
        public string Nach { get; private set; }
        public int Laenge { get; private set; }

        public Strasse(string von, string nach, int laenge)
        {
            this.Von = von;
            this.Nach = nach;
            this.Laenge = laenge;
        }
    }
}
