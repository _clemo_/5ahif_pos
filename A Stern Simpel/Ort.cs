﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace A_Stern_Simpel
{
    class Ort
    {
        public string Bezeichnung { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }

        public Ort(string Bezeichnung, int X, int Y)
        {
            this.Bezeichnung = Bezeichnung;
            this.X = X;
            this.Y = Y;
        }
    }
}
