﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace A_Stern_Simpel
{
    class AStern
    {
        private HashTable<string, Ort> Orte;
        private HashTable<string, VerketteteListe<Strasse>> Strassen;

        public AStern(HashTable<string, Ort> orte, HashTable<string, VerketteteListe<Strasse>> strassen)
        {
            Orte = orte;
            Strassen = strassen;
        }

        public VerketteteListe<Ort> FindeWeg(string von, string nach)
        {
            OpenList OL = new OpenList(Orte.Get(nach));
            HashTable<string, ClosedListNode> CL = new HashTable<string, ClosedListNode>(Orte.Size);

            OL.Add(new OpenListNode() { BisherigerWeg = 0, WegZuPunkt = new Strasse(von,von,0), Knoten = Orte.Get(von), Schaezung = 0 });

            OpenListNode aktuellerKnoten = OL.Read();
            while (aktuellerKnoten != null)
            {
                //erzeuge Eintrag für ClosedList
                ClosedListNode cn = new ClosedListNode();
                cn.Knoten = aktuellerKnoten.Knoten;
                cn.gekommenVon = Orte.Get(aktuellerKnoten.WegZuPunkt.Von);
                cn.Weg = aktuellerKnoten.BisherigerWeg + aktuellerKnoten.WegZuPunkt.Laenge;

                // prüfe/ändere CL
                try
                {
                    var bisherigerEintrag = CL.Get(cn.Knoten.Bezeichnung);
                    if (bisherigerEintrag.Weg > cn.Weg)
                    {
                        bisherigerEintrag.Weg = cn.Weg;
                        bisherigerEintrag.gekommenVon = cn.gekommenVon;
                    }
                }
                catch (ArgumentException)
                {
                    CL.Put(cn.Knoten.Bezeichnung, cn);
                }

                if (aktuellerKnoten.Knoten.Bezeichnung.Equals(nach))
                {
                    break;
                }

                // suche alle Straßen die von diesem Punkt wegführen
                var strassenWegVonPunkt = Strassen.Get(aktuellerKnoten.Knoten.Bezeichnung);

                // bereite strassen für OL vor und füge in OL ein
                while (strassenWegVonPunkt != null)
                {
                    var strasse = strassenWegVonPunkt.Value;
                    strassenWegVonPunkt = strassenWegVonPunkt.Next;

                    OpenListNode on = new OpenListNode();
                    on.Knoten = Orte.Get(strasse.Nach);
                    on.BisherigerWeg = cn.Weg;
                    on.WegZuPunkt = strasse;

                    OL.Add(on);
                }
                aktuellerKnoten = OL.Read();
            }
            if (aktuellerKnoten == null)
                return null;

            // gehe weg "zurück"
            VerketteteListe<Ort> root = null;
            string aktuell = nach;
            do{
                var CL_Eintrag = CL.Get(aktuell);
                var node = new VerketteteListe<Ort>() { Value = CL_Eintrag.Knoten, Next = root };
                root = node;

                aktuell = CL_Eintrag.gekommenVon.Bezeichnung;
            }while(aktuell != von);

            var nodeStart = new VerketteteListe<Ort>() { Value = Orte.Get(von), Next = root };
            return nodeStart;
        }

        class OpenListNode
        {
            public Ort Knoten { get; set; }
            public double BisherigerWeg { get; set; }
            public Strasse WegZuPunkt { get; set; }

            public double Schaezung = -1;
            public void BerechneSchaezung(Ort Ziel)
            {
                int[] vektor = new int[]{(Ziel.X-Knoten.X), (Ziel.Y-Knoten.Y)};
                double luftlinie = Math.Sqrt(vektor[0] * vektor[0] + vektor[1] * vektor[1]);
                this.Schaezung = BisherigerWeg + WegZuPunkt.Laenge + luftlinie;
            }
        }

        class ClosedListNode
        {
            public Ort Knoten;
            public Ort gekommenVon;
            public double Weg;
        }

        class OpenList
        {
            VerketteteListe<OpenListNode> OffneKnoten = null;
            Ort Ziel = null;

            public OpenList(Ort Ziel)
            {
                this.Ziel = Ziel;
            }

            public void Add(OpenListNode n)
            {
                if (n.Schaezung == -1)
                    n.BerechneSchaezung(Ziel);

                if (OffneKnoten == null)
                {
                    OffneKnoten = new VerketteteListe<OpenListNode>();
                    OffneKnoten.Value = n;
                    OffneKnoten.Next = null;
                    return;
                }
                else
                {
                    if (OffneKnoten.Value.Schaezung > n.Schaezung)
                    {
                        var ListenElement = new VerketteteListe<OpenListNode>();
                        ListenElement.Value = n;
                        ListenElement.Next = OffneKnoten;
                        OffneKnoten = ListenElement;
                        return;
                    }
                    else
                    {
                        VerketteteListe<OpenListNode> i = OffneKnoten;
                        while (i.Next != null && i.Next.Value.Schaezung < n.Schaezung)
                            i = i.Next;
                        if (i.Next == null)
                        {
                            i.Next = new VerketteteListe<OpenListNode>();
                            i.Next.Value = n;
                            i.Next.Next = null;
                            return;
                        }
                        else
                        {
                            var ListenElement = new VerketteteListe<OpenListNode>();
                            ListenElement.Value = n;
                            ListenElement.Next = i.Next;
                            i.Next = ListenElement;
                            return;
                        }
                    }
                }
            }

            public OpenListNode Read()
            {
                if (OffneKnoten == null) return null;
                OpenListNode ret = OffneKnoten.Value;
                OffneKnoten = OffneKnoten.Next;
                return ret;
            }
        }
    }
}
