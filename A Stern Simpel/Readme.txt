﻿A Stern Step by Step:
1. Klassen
	Da Orte und Straßen abgespeichert werden, werden Klassen für Ort und Straße angelegt.
	Ein Ort hat eine Bezeichnung und eine Koordinate
	Eine Straße Verbindet zwei Orte und hat eine Länge

1.1 Ort
	Es wird eine Klasse Ort angelegt. 
	(Optional: Da sich der Ort nicht verändert, sind die Eigenschaften des Ortes von außerhalb nicht änderbar.)
	3 Properties anlegen (prop[Tab][Tab] oder wenn außen nicht veränderbar propg[Tab[Tab])
	string Bezeichnung
	int X
	int Y

	Ein Konstruktor anlegen der diese Werte setzt (ctor[Tab][Tab] erzeugt leeren Konstruktor)

1.2 Straße
	Es wird eine Klasse Strasse angelegt.
	(Optional: Da sich die Straße nicht verändert, sind die Eigenschaften der Straße von außerhalb nicht änderbar.)
	3 Properties anlegen (prop[Tab][Tab] oder wenn außen nicht veränderbar propg[Tab[Tab])
	string von
	string nach
	int laenge

	Ein Konstruktor anlegen der diese Werte setzt (ctor[Tab][Tab] erzeugt leeren Konstruktor)

2. Definieren welche Datenstrukturen benötigt werden.
	Es müssen alle Orte abgespeichert werden, da Straßen nur die Bezeichnungen der Orte speichern, wäre es vorteilhaft diese als Key zu nutzen um 
	schnell darauf zuzugreifen.
	Es müssen weiters alle Straßen abgespeichert werden. (ka wie genau tb die in eine HT speichern wollte). Da gesucht wird welche Straßen von 
	einem Punkt wegführen, wird der Ausgangspunkt als Key genutzt. Da (hoffentlich) mehrere Straßen von einem Punkt wegführen, sollten diese unter 
	dem Schlüssel zusammengefasst werden. Für das Zusammenfassen wird eine verkettete Liste genutzt. Also eine Hashtable, deren Key der Ausgangs-
	Punkt ist und der Value eine Liste an Straßen.

2.1 Implementieren einer Hashtable (extern verkettet)
	Es wird eine Klasse HashTable angelegt. Die Klasse benötigt 2 Generische Typen: einen für den Key und einen für den Value.
	Diese Typen werden Key_t und Value_t genannt (_t kommt aus c++/c und weißt darauf hin, dass es sich um Datentypen handelt)
	In einer Hash-Tablle wird immer Key+Value abgespeichert. Daher zuerst innerhalb der Hashtabelle eine Klasse für Key+Value anlegen.
	Da die Hashtabelle extern Verkettet wird, noch eine Klasse für ein Element einer verketteten Liste anlegen.

	Eine Hash-Tabelle verwaltet immer ein Array einer defineirten Größe 
	> private Membervariable für Größe anlegen sowie Konstruktor der Varible Wert zuweist.
	> Array vom Typ KeyValueListItem anlegen und im Konstruktor mit der angegebenen Größe instanziieren.
		>> Wichtig ist immer beim Anlegen von Arrays, alle Felder mit einem Ausgangs-Wert zu füllen. In dem Fall null

	In einer Hashtabelle müssen Daten gespeichert werden 
		> Funktion zum Speichern anlegen
		> Key+Value Objekt erstellen
		> Hash von Key bilden
			>> In C# gibt es in jedem Datentyp die Funktion GetHashCode() welche einen int zurücklifert
			>> int kann auch negativ sein -> das wollen wir nicht daher, wenn negativ *-1
		> Hash hat jetzt Objekt > int gemacht, dieser int hat nun einen Bereich von 0 - 2^32 daher % ArrayGröße um Wert zwischen 0 und Arraygröße zu bekommen.
		> Prüfen ob so berechnerter Index im Array leer ist
			>> wenn ja -> KeyValuePairListItem anlegen und mit Daten füllen
			>> wenn nein -> KeyValuePairListItem anlegen und an Beginn der Liste stellen.

	Aus einer Hashtabelle müssen Daten gelesen werden können
		> Funktion zum Auslesen anlegen
		> Hash von Key bilden
			> wenn < 0 * -1
			> Hash % ArrayGröße
		> prüfe Index an berechneter Stelle
			> Wenn auf berechneter Postiion kein Element gefunden > Exception oder default zurückgeben
			> Liste an dieser Position durchgen bis Ende der Liste erreicht 
				> Key gefunden > Value zurückgeben
				> Key nicht gefunden > Exception werfen
2.2 Klasse für verkettete Liste erstellen
	*man legt die verkettete Liste sinnvollerweise VOR der HashTable an um sie dann in der Hashtable zu nutzen.
        Habe es deswegen nicht gemacht, da ich die Hashtable und die LinkedList komplett getrennt haben wollte
				
3. Klasse für AStern anlegen.
	Die Klasse benötigt einen Start- und einen Ziel-Punkt. Sowie alle Punkte und Straßen.

3.1 Zusätzliche Klassen
	Im A* wird eine s.g. OpenList gefpührt mit allen noch offenen Punkten.
	Sowie eine ClosedList mit schon geprüften Punkten
3.1.1 OpenListNode
	Für jeden Punkt in der Open List wird gespeichert über welche Straße er erreicht wurde sowie eine Schätzung des Weges bis zum Ziel.
		Die Schätzung berechnet sich aus Bisher zurückgelegter Weg + Weg zu diesem Punkt + Luftlinie vom Punkt zum Ziel

3.1.2 ClosedListNode
	Für jeden Punkt in der Closed List wird gespeichert von welchem Punkt aus man zu diesem Punkt gekommen ist, sowie der Weg bis dorthin.

3.2 Finde Weg
	Es wird in der A* Klasse eine Funktion angelet, welche von und nach (als string) bekommt und eine Liste an Punkten zurückliefert

	In dieser Funktion wird eine Open und eine Closed-List angelegt.
	Die Open-List ist eine Sortierte Liste, die Closed eine Hashtable deren Key der Ort ist (es muss ja schnell geprüft werden 
	können wie man zu einem Punkt kommt)

3.2.1 Sortierte Liste
	Da die Open List eine Sortierte Liste ist, wird hierfür eine Klasse angelegt mit der schnell OpenList-Nodes sortiert eingefügt werden können, 
		sortiert wird nach der Schätzfunktion, gelesen/gelöscht wird immer das erste Element.
		Damit geschätzt werden kann, muss der OpenList der Ziel-Ort bekannt sein.

3.2.1 Anlegen der Open und Closed List.
	Die Klasse für die Open list wurde vorher geschrieben, die ClosedList ist eine HashTable mit Ort (string) und einem ClosedList-Node - die Größe 
		ist an sich nebenächlich, da diese nur über die Performance entscheided 
			höhrerer Wert = mehr ram + mehr performance
			niedriger Wert = weniger ram + weniger performance

			ich gehe vom durchschnitts-Fall aus, dass jeder knoten nur einmal besicht wird, und nehme die Anzahl an Orten

	Die Open List muss mit dem Ausgangs-Knoten befüllt werden!

3.2.2 Der Algoritmus:
	Solange noch ein Knoten in der Open list ist
		> nimm den Knoten
		> wenn Knoten == Ziel > fertig
		> wenn Knoten != Ziel
		> Rufe alle Knoten ab die von diesem Punkt aus erreicht werden können und gib sie in die OpenList
		> Gibt es bereits aktuellen Knoten in der Closed List
			> vergleiche Weg bis zu Punkt, ist aktueller Weg kürzer -> Tausche den von Punkt und aktualisiere den Weg
		> Noch nicht in ClosedList enthalten 
			> schreibe in Closed List

3.2.3 rückgabe
	jetzt wird die closed-list wieder "zurück" gegeangen, es wird vom ziel-punkt aus gegangen und über die gespeicherten wege
		zurück gegangen, durch das umschreiben der CL in 3.2.2 ist sichergestellt dass immer der schnellste weg zu einem Punkt abgespeichert wurde

4. Testen
	Beim Testen, sollte beachtet werden, dass im Algorithmus jede Kante gerichtet ist, da wir davon ausgehen, dass Straßen 
	in beide Richtungen befahren werden können, werden sie Doppelt eingefügt

	Der Testgraph ist dem nachempfunden, den TB einmal an die Tafel gezeichnet hat.

	Die Test- / Ausgabe-Funktionen sind tum Testen einer eigenen Implementierung gedacht.

		
