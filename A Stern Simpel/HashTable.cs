﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace A_Stern_Simpel
{
    class HashTable<Key_t, Value_t>
    {
        public int Size { get; private set; }
        private KeyValueListItem[] Table;

        public HashTable(int size)
        {
            this.Size = size;
            Table = new KeyValueListItem[size];
            for (int i = 0; i < size; i++)
            {
                Table[i] = null;
            }
        }

        public void Put(Key_t Key, Value_t Value)
        {
            KeyValue data = new KeyValue();
            data.Key      = Key;
            data.Value    = Value;

            int hash = Key.GetHashCode();
            if (hash < 0)
                hash *= -1;

            int idx = hash % Size;

            if (Table[idx] == null)
            {
                Table[idx] = new KeyValueListItem();
                Table[idx].Pair = data;
                Table[idx].Next = null;
            }
            else
            {
                var temp = new KeyValueListItem();
                temp.Pair = data;
                temp.Next = Table[idx];
                Table[idx] = temp;
            }
        }

        public Value_t Get(Key_t Key)
        {
            int hash = Key.GetHashCode();
            if (hash < 0)
                hash *= -1;
            int idx = hash % Size;

            if (Table[idx] == null)
            {
                throw (new ArgumentException("Key nicht vorhanden!"));
            }
            else
            {
                var item = Table[idx];
                while (item != null)
                {
                    if (item.Pair.Key.Equals(Key))
                    {
                        return item.Pair.Value;
                    }
                    item = item.Next;
                }
                throw (new ArgumentException("Key nicht vorhanden!"));
            }
        }

        class KeyValue
        {
            public Key_t Key;
            public Value_t Value;
        }

        class KeyValueListItem
        {
            public KeyValue Pair;
            public KeyValueListItem Next;
        }
    }
}
