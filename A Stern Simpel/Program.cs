﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace A_Stern_Simpel
{
    class Program
    {
        static void addStreet(Strasse s, HashTable<string, VerketteteListe<Strasse>> Strassen)
        {
            VerketteteListe<Strasse> liste;
            try
            { // straße bereits in hastabelle?
                liste = Strassen.Get(s.Von);

                // füge am ende ein
                while (liste.Next != null)
                    liste = liste.Next;
                liste.Next = new VerketteteListe<Strasse>() { Value = s, Next = null };
            }
            catch(ArgumentException)
            {
                Strassen.Put(s.Von, new VerketteteListe<Strasse>() { Value = s, Next = null });
            }
        }
        
        static void FuegeStrasseEin(string von, string nach, HashTable<string, Ort> Orte, HashTable<string, VerketteteListe<Strasse>> Strassen, int laenge = -1)
        {
            if(laenge == -1){
                int x = Orte.Get(nach).X-Orte.Get(von).X;
                int y = Orte.Get(nach).Y-Orte.Get(von).Y;
                laenge = Convert.ToInt32(Math.Sqrt(x*x+y*y));
            }
            Strasse ab = new Strasse(von, nach, laenge);
            Strasse ba = new Strasse(nach, von, laenge);

            addStreet(ab, Strassen);
            addStreet(ba, Strassen);
        }

        #region Consolen Zeichnung
        static void PrintPosition(int X, int Y, string C)
        {
            Console.SetCursorPosition(X, Y);
            Console.Write(C);
        }

        static void printMap()
        {
            PrintPosition(2 , 14 - 7, "A");
            PrintPosition(7 , 14 - 8, "B");
            PrintPosition(14, 14 - 6, "C");
            PrintPosition(13, 14 - 14, "D");
            PrintPosition(6 , 14 - 3, "E");
            PrintPosition(10, 14 - 1, "F");
            PrintPosition(15, 14 - 1, "G");
            PrintPosition(20, 14 - 0, "H");
            PrintPosition(0 , 14 - 11, "I");
            PrintPosition(20, 14 - 7, "Y");
            PrintPosition(18, 14 - 5, "Z");
        }

        static void PrintConnection(string von, string nach, HashTable<string, Ort> Orte)
        {
            Ort _von = Orte.Get(von);
            Ort _nach = Orte.Get(nach);

            int[] v = new int[] { _nach.X - _von.X, _nach.Y - _von.Y };
            double l = Math.Sqrt(v[0] * v[0] + v[1] * v[1]);
            double[] e = new double[] { v[0] / l, v[1] / l };

            double[] pos = new double[] { _von.X, _von.Y };
            int[] iPos = new int[] { _von.X, _von.Y };
            while (iPos[0] != _nach.X || iPos[1] != _nach.Y)
            {
                pos = new double[] { pos[0] + e[0], pos[1] + e[1] };
                iPos = new int[] { Convert.ToInt32(Math.Round(pos[0], 0)), Convert.ToInt32(Math.Round(pos[1], 0)) };


                if ((iPos[0] != _von.X || iPos[1] != _von.Y) && (iPos[0] != _nach.X || iPos[1] != _nach.Y))
                {
                    Console.SetCursorPosition(iPos[0], 14-iPos[1]);
                    Console.Write('+');
                }
            }
        }

        static void PrintConnections(HashTable<string, Ort> Orte){
            PrintConnection("A","B", Orte);
            PrintConnection("A","E", Orte);
            PrintConnection("A","I", Orte);
            PrintConnection("I","Y", Orte); // not direct connected 
            PrintConnection("E","F", Orte);
            PrintConnection("E","B", Orte);
            PrintConnection("F","G", Orte);
            PrintConnection("F","C", Orte);
            PrintConnection("G","C", Orte);
            PrintConnection("G","H", Orte);
            PrintConnection("Z","H", Orte);
            PrintConnection("C","Z", Orte);
            PrintConnection("B","D", Orte);
            PrintConnection("B", "C", Orte);
            PrintConnection("D","Z", Orte);
        }

        static string[] AskRoute()
        {
            Console.SetCursorPosition(0, 22);
            Console.Write("Wo soll gestartet werden? ");
            string s = Console.ReadKey().KeyChar.ToString();
            Console.WriteLine();
            Console.Write("Ziel? ");
            string e = Console.ReadKey().KeyChar.ToString();
            Console.WriteLine();
            return new string[] { s, e };
        }
        #endregion

        static void Main(string[] args)
        {
            HashTable<string, Ort> Orte = new HashTable<string, Ort>(10);
            Orte.Put("A",new Ort("A",2, 7));
            Orte.Put("B",new Ort("B",7, 8));
            Orte.Put("C",new Ort("C",14, 6));
            Orte.Put("D",new Ort("D",13, 14));
            Orte.Put("E",new Ort("E",6, 3));
            Orte.Put("F",new Ort("F",10, 1));
            Orte.Put("G",new Ort("G",15, 1));
            Orte.Put("H",new Ort("H",20, 0));
            Orte.Put("I",new Ort("I",0, 11));
            Orte.Put("Y",new Ort("Y",20, 7));
            Orte.Put("Z",new Ort("Z",18, 5));

            HashTable<string, VerketteteListe<Strasse>> Strassen = new HashTable<string, VerketteteListe<Strasse>>(40);

            FuegeStrasseEin("A","B",Orte,Strassen);
            FuegeStrasseEin("A","E",Orte,Strassen);
            FuegeStrasseEin("A","I",Orte,Strassen);
            FuegeStrasseEin("I","Y",Orte,Strassen, 82); // 4xluftlinie
            FuegeStrasseEin("E","F",Orte,Strassen);
            FuegeStrasseEin("E","B",Orte,Strassen);
            FuegeStrasseEin("F","G",Orte,Strassen);
            FuegeStrasseEin("F","C",Orte,Strassen);
            FuegeStrasseEin("G","C",Orte,Strassen);
            FuegeStrasseEin("G","H",Orte,Strassen);
            FuegeStrasseEin("Z","H",Orte,Strassen);
            FuegeStrasseEin("C","Z",Orte,Strassen);
            FuegeStrasseEin("B","D",Orte,Strassen);
            FuegeStrasseEin("B", "C",Orte,Strassen);
            FuegeStrasseEin("D","Z",Orte,Strassen);

            AStern ast = new AStern(Orte, Strassen);

            printMap();
            PrintConnections(Orte);
            var input = AskRoute();
            var route = ast.FindeWeg(input[0], input[1]);
            //var route = ast.FindeWeg("A", "Z");


            if(route != null)
                Console.Write(route.Value.Bezeichnung);
            route = route.Next;
            while(route != null){
                Console.Write(" > ");
                Console.Write(route.Value.Bezeichnung);
                route = route.Next;
            }
            Console.ReadLine();
        }
    }
}
