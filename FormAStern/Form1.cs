﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using ConsoleAStern;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Threading;

namespace FormAStern
{
    public partial class Form1 : Form
    {
        private Exchange exchange = null;
        private ContextMenu cmPoints = null;
        private ContextMenu cmConns = null;
        private Graphics graphics = null;
        private Bitmap _bmp = null;
        private Punkt start = null;
        private bool loaded = false;

        private Exchange Scale(Exchange stat, double factor) //nur weil ursprungsdaten zu groß für meinen laptop-bildschrm waren!
        {
            Exchange ret = new Exchange();
            ret.ImgPath = stat.ImgPath;
            List<Punkt> pt = new List<Punkt>();
            foreach (var item in stat.Punkte)
            {
               pt.Add(new Punkt(Convert.ToInt32(Math.Round(item.X * factor,0)), Convert.ToInt32(Math.Round(item.Y * factor,0)), item.Bezeichnung));
            }

            List<Verbindung> con = new List<Verbindung>();
            foreach (var item in stat.Verbindungen)
            {
                con.Add(new Verbindung(item.VON, item.NACH, item.LENGTH * factor));
            }

            ret.Punkte = pt;
            ret.Verbindungen = con;
            return ret;
        }

        private Exchange DEFAULTDATA()
        {
            Exchange ret = new Exchange();
            ret.ImgPath = @"Default.png";
            ret.Punkte = new List<Punkt>();
            ret.Verbindungen = new List<Verbindung>();

            // stadt-maker
            ret.Punkte.Add(new Punkt(246, 104, "S1.1"));
            ret.Punkte.Add(new Punkt(80, 185, "S1.2"));// connected
            ret.Punkte.Add(new Punkt(94, 396, "S2.1")); // connected
            ret.Punkte.Add(new Punkt(232, 420, "S2.2")); // connected
            ret.Punkte.Add(new Punkt(234, 710, "S2.3")); // connected
            ret.Punkte.Add(new Punkt(400, 530, "S3.1")); // connected
            ret.Punkte.Add(new Punkt(540, 590, "S3.2")); // connected
            ret.Punkte.Add(new Punkt(855, 570, "S4.1"));// connected
            ret.Punkte.Add(new Punkt(971, 562, "S4.2"));// connected
            ret.Punkte.Add(new Punkt(916, 877, "S5.1")); // connected
            ret.Punkte.Add(new Punkt(1040, 870, "S5.2")); // connected
            ret.Punkte.Add(new Punkt(1195, 500, "S6.1")); // connected
            ret.Punkte.Add(new Punkt(1182, 388, "S6.2")); // connected
            ret.Punkte.Add(new Punkt(1195, 310, "S6.3")); // connected

            // kloster-marker
            ret.Punkte.Add(new Punkt(85, 734, "KL1")); // connected
            ret.Punkte.Add(new Punkt(82, 905, "KL2")); // NOT connected
            ret.Punkte.Add(new Punkt(560, 890, "KL3")); // connected
            ret.Punkte.Add(new Punkt(545, 245, "KL4")); // connected
            ret.Punkte.Add(new Punkt(714, 400, "KL5")); // connected
            ret.Punkte.Add(new Punkt(885, 240, "KL6")); // connected
            ret.Punkte.Add(new Punkt(710, 750, "KL7")); // connected

            // kurven-marker
            ret.Punkte.Add(new Punkt(405, 882, "K1")); // connected
            ret.Punkte.Add(new Punkt(720, 560, "K2")); // connected
            ret.Punkte.Add(new Punkt(1200, 85, "K3")); // connected
            ret.Punkte.Add(new Punkt(1035, 718, "K4")); // connected
            ret.Punkte.Add(new Punkt(1200, 718, "K5")); // connected
            ret.Punkte.Add(new Punkt(398, 242, "K6")); // connected
            ret.Punkte.Add(new Punkt(874, 393, "K7")); // connected
            ret.Punkte.Add(new Punkt(714, 238, "K8")); // connected
            

            // kreuzungs-marker
            ret.Punkte.Add(new Punkt(409, 728, "KR1")); // connected
            ret.Punkte.Add(new Punkt(712, 900, "KR2")); // connected
            ret.Punkte.Add(new Punkt(570, 410, "KR3")); // connected
            ret.Punkte.Add(new Punkt(400, 376, "KR4")); // connected
            ret.Punkte.Add(new Punkt(1030, 390, "KR5")); // connected
            ret.Punkte.Add(new Punkt(720, 80, "KR6")); // connected
            ret.Punkte.Add(new Punkt(1045, 75, "KR7")); // connected

            // Verbindungen
            ret.Verbindungen.Add(new Verbindung("S1.1", "S1.2"));
            ret.Verbindungen.Add(new Verbindung("S1.2", "S2.1"));
            ret.Verbindungen.Add(new Verbindung("S2.1", "S2.2"));
            ret.Verbindungen.Add(new Verbindung("S2.1", "S2.3"));
            ret.Verbindungen.Add(new Verbindung("S2.2", "S2.3"));
            ret.Verbindungen.Add(new Verbindung("S2.3", "KL1"));
            ret.Verbindungen.Add(new Verbindung("S2.3", "KR1"));
            ret.Verbindungen.Add(new Verbindung("KR1", "S3.1"));
            ret.Verbindungen.Add(new Verbindung("S3.1", "S3.2"));
            ret.Verbindungen.Add(new Verbindung("S4.1", "S4.2"));
            ret.Verbindungen.Add(new Verbindung("S5.1", "S5.2"));
            ret.Verbindungen.Add(new Verbindung("S6.1", "S6.2"));
            ret.Verbindungen.Add(new Verbindung("S6.2", "S6.3"));
            ret.Verbindungen.Add(new Verbindung("KR2", "KL7"));
            ret.Verbindungen.Add(new Verbindung("KR2", "KL3"));
            ret.Verbindungen.Add(new Verbindung("KR2", "S5.1"));
            ret.Verbindungen.Add(new Verbindung("KR3", "KL5"));
            ret.Verbindungen.Add(new Verbindung("KR3", "S3.2"));
            ret.Verbindungen.Add(new Verbindung("KR3", "KR4"));
            ret.Verbindungen.Add(new Verbindung("KR4", "S2.2"));
            ret.Verbindungen.Add(new Verbindung("S4.1", "K2"));
            ret.Verbindungen.Add(new Verbindung("KR5", "S4.2"));
            ret.Verbindungen.Add(new Verbindung("KR5", "S6.2"));
            ret.Verbindungen.Add(new Verbindung("KR5", "KR7"));
            ret.Verbindungen.Add(new Verbindung("K4", "S5.2"));
            ret.Verbindungen.Add(new Verbindung("K5", "K4"));
            ret.Verbindungen.Add(new Verbindung("K5", "S6.1"));
            ret.Verbindungen.Add(new Verbindung("K1", "KL3"));
            ret.Verbindungen.Add(new Verbindung("K1", "KR1"));
            ret.Verbindungen.Add(new Verbindung("KL4", "K6"));
            ret.Verbindungen.Add(new Verbindung("KL4", "K8"));
            ret.Verbindungen.Add(new Verbindung("KR6", "K8"));
            ret.Verbindungen.Add(new Verbindung("KR6", "S1.1"));
            ret.Verbindungen.Add(new Verbindung("KR6", "KR7"));
            ret.Verbindungen.Add(new Verbindung("KR4", "K6"));
            ret.Verbindungen.Add(new Verbindung("K7", "KL5"));
            ret.Verbindungen.Add(new Verbindung("K2", "KL5"));
            ret.Verbindungen.Add(new Verbindung("K7", "KL6"));
            ret.Verbindungen.Add(new Verbindung("K2", "S4.1"));
            ret.Verbindungen.Add(new Verbindung("K3", "KR7"));
            ret.Verbindungen.Add(new Verbindung("K3", "S6.3"));
            return ret;
        }

        private void UpdateListBox()
        {
            lbPoints.Items.Clear();
            foreach (var item in exchange.Punkte)
            {
                lbPoints.Items.Add(item);
            }

            lbConnections.Items.Clear();
            foreach (var item in exchange.Verbindungen)
            {
                lbConnections.Items.Add(item);
            }
        }

        public Form1()
        {
            InitializeComponent();
            tabControl1.ChangeUICues += tabControl1_ChangeUICues;
            var cm  = new ContextMenu();
                var addItm = new MenuItem("add");
                    addItm.Click += p_addItm_Click;
                cm.MenuItems.Add(addItm);

                var delItm = new MenuItem("delete");
                    delItm.Click += p_delItm_Click;
                cm.MenuItems.Add(delItm);
            lbPoints.ContextMenu = cm;

            var cm2 = new System.Windows.Forms.ContextMenu();
                var addItm_v = new MenuItem("add");
                    addItm.Click += v_addItm_Click;
                cm2.MenuItems.Add(addItm);

                var delItm_v = new MenuItem("delete");
                    delItm.Click += v_delItm_Click;
                cm2.MenuItems.Add(delItm);
            lbConnections.ContextMenu = cm2;

            cmPoints = cm;
            cmConns = cm2;

            //exchange = DEFAULTDATA();
        }

        void v_addItm_Click(object sender, EventArgs e)
        {
            if (!cmConns.MenuItems.Contains(sender as MenuItem)) return;
            AddCon ac = new AddCon(exchange.Punkte, exchange.Verbindungen);
            ac.ShowDialog();
            if (ac.CONN != null)
            {
                exchange.Verbindungen.Add(ac.CONN);
                UpdateListBox();
            }
        }

        void v_delItm_Click(object sender, EventArgs e)
        {
            if (!cmConns.MenuItems.Contains(sender as MenuItem)) return;
            if (lbPoints.SelectedItem != null && lbPoints.SelectedItem is Verbindung)
            {
                exchange.Verbindungen.Remove(lbPoints.SelectedItem as Verbindung);
                UpdateListBox();
            }
        }

        void p_delItm_Click(object sender, EventArgs e)
        {
            if (!cmPoints.MenuItems.Contains(sender as MenuItem)) return;
            if (lbPoints.SelectedItem != null && lbPoints.SelectedItem is Punkt)
            {
                exchange.Punkte.Remove(lbPoints.SelectedItem as Punkt);
                UpdateListBox();
            }
        }

        void p_addItm_Click(object sender, EventArgs e)
        {
            if (!cmPoints.MenuItems.Contains(sender as MenuItem)) return;
            AddPoint ap = new AddPoint(exchange.Punkte);
            ap.ShowDialog();
            if (ap.RET != null)
            {
                exchange.Punkte.Add(ap.RET);
                UpdateListBox();
            }
        }

        void tabControl1_ChangeUICues(object sender, UICuesEventArgs e)
        {
            if (exchange == null)
                btnLoad_Click(sender, e);
            if (exchange != null)
            {
                UpdateListBox();
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog import = new OpenFileDialog();
            import.Filter = "Exchange|*.xml;*.bin";
            import.Title = "Auswahl des Exchange-Fiels";
            var result = import.ShowDialog();
            if (result != DialogResult.OK) // keine Datei ausgewählt
            {
                if (exchange == null) // noch keine Daten vorhanden
                {
                    var res = MessageBox.Show("Es sind keine Daten vorhanden!\nSetup starten?", "Keine Daten vorhanden!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (res == DialogResult.Yes)
                    {
                        btnNew_Click(sender, e);
                    }
                    else
                        return;
                }
                return;
            }
            if (import.FileName.EndsWith(".xml"))
            {
                XmlSerializer xs = new XmlSerializer(typeof(Exchange));
                using (StreamReader sr = new StreamReader(import.FileName))
                {
                    exchange = (Exchange)xs.Deserialize(sr);
                }
            }
            else
            {
                BinaryFormatter bf = new BinaryFormatter();
                using (StreamReader sr = new StreamReader(import.FileName))
                {
                    exchange = (Exchange) bf.Deserialize(sr.BaseStream);
                }
            }

            foreach (var item in exchange.Verbindungen)
            {
                Verbindung._add(item.VON, item);
                Verbindung._add(item.NACH, item);
            }

            foreach (var item in exchange.Punkte)
            {
                Punkt.AllePunkte.Add(item.Bezeichnung, item);
            }
        }

        private void btnSaveData_Click(object sender, EventArgs e)
        {
            SaveFileDialog export = new SaveFileDialog();
            export.Title = " Speicherort für Exchange-Datei";
            export.Filter = "Exchange|*.xml;*.bin";
            export.ShowDialog();
            if (export.FileName.EndsWith(".xml"))
            {
                XmlSerializer xs = new XmlSerializer(typeof(Exchange));
                using (StreamWriter sr = new StreamWriter(export.FileName,false))
                {
                    xs.Serialize(sr, exchange);
                    sr.Flush();
                }
            }
            else
            {
                BinaryFormatter bf = new BinaryFormatter();
                using (StreamWriter sr = new StreamWriter(export.FileName))
                {
                    bf.Serialize(sr.BaseStream, exchange);
                    sr.Flush();
                }
            }
        }

        private void btnLoading_Click(object sender, EventArgs e)
        {
            while (exchange == null)
            {
                var res = MessageBox.Show("Es sind keine Daten vorhanden!\nSetup starten?", "Keine Daten vorhanden!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (res == DialogResult.Yes)
                {
                    btnNew_Click(sender, e);
                }
            }
            if(exchange != null)
            {
                if (exchange.ImgPath == null || exchange.ImgPath.Trim().Length == 0 || !File.Exists(exchange.ImgPath))
                {
                    MessageBox.Show("Es wurde keine Karten-Datei gefunen!", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                picMaster.Load(exchange.ImgPath);

                this.Width = picMaster.Image.Width + 50;
                this.Height = picMaster.Image.Height + 70;
                tabControl1.Height = picMaster.Image.Height + 50;
                tabControl1.Width = picMaster.Image.Width + 30;
                tabPage2.Width = picMaster.Image.Width + 40;
                tabPage2.Height = picMaster.Image.Height + 40;
                picMaster.Height = picMaster.Image.Height;
                picMaster.Width = picMaster.Image.Width;

                Bitmap _bmp = new Bitmap(picMaster.Image);
                graphics = Graphics.FromImage(_bmp);
                graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                picMaster.Image = _bmp;
                foreach (var item in exchange.Punkte)
                {
                    Draw(item);
                }
                picMaster.Refresh();
                loaded = true;

                ThreadPool.QueueUserWorkItem(
                    (o) =>
                    {
                        MessageBox.Show("Anklicken von 2 Punkten nacheinander wird eine Route geplant. \nDurch rechtsklicken kann ein Punkt erstellt werden.\n Durch das nacheinander rechsklicken von Punkten können Verbindungen erstellt werden.", "Hinweis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    });
            }
        }

        
        private void Draw(Verbindung v, Pen p = null)
        {
            if(p == null) p = new Pen(Color.Black,4.0f);

            Punkt a = v.Other(v.NACH), b = v.Other(v.VON);
            Draw(a, b, p);
        }

        private void Draw(Punkt a, Pen p = null)
        {
            Pen pn = p ?? new Pen(Brushes.Blue,2.0f);

            graphics.DrawEllipse(pn, a.X - exchange.AreaSize / 2, a.Y - exchange.AreaSize / 2, exchange.AreaSize, exchange.AreaSize);
        }

        private void Draw(Punkt a, Punkt b, Pen p = null)
        {
            Point ap = new Point(a.X, a.Y), bp = new Point(b.X, b.Y);

            graphics.DrawLine(p, ap, bp);
            picMaster.Refresh();
        }

        private Punkt FindFirstPointInRange(int x, int y, double radius){
            var l = exchange.Punkte.Where((p) => {
                var vektLen = Math.Sqrt(Math.Pow(p.X - x, 2) + Math.Pow(p.Y - y, 2));
                return vektLen <= radius;
            });
            if (l.Count() == 0) return null;
            else if (l.Count() > 1)
            {
                var li = l.ToList();
                li.Sort(
                    (a, b) => { return (Math.Sqrt(Math.Pow(a.X - x, 2) + Math.Pow(a.Y - y, 2))).CompareTo(
                        Math.Sqrt(Math.Pow(b.X - x, 2) + Math.Pow(b.Y - y, 2))); 
                    });
                return li.First();
            }
            return l.First();
        }

        private void picMaster_Click(object sender, EventArgs e)
        {
            var m = e as MouseEventArgs;

            if (!loaded) return;
            if (m.Button == System.Windows.Forms.MouseButtons.Right)
            {
                var inRange = FindFirstPointInRange(m.X, m.Y, exchange.AreaSize/2);
                if (inRange == null)
                {
                    AddPoint ap = new AddPoint(exchange.Punkte, m.X, m.Y);
                    ap.ShowDialog();
                    if (ap.RET != null)
                    {
                        exchange.Punkte.Add(ap.RET);
                        UpdateListBox();
                        Draw(ap.RET);
                        picMaster.Refresh();
                    }
                    return;
                }
                else
                {
                    if (start == null)
                    {
                        start = inRange;
                    }
                    else
                    {
                        AddCon ac = new AddCon(exchange.Punkte, exchange.Verbindungen, start, inRange);
                        ac.ShowDialog();

                        if (ac.CONN != null)
                        {
                            exchange.Verbindungen.Add(ac.CONN);
                            UpdateListBox();
                        }
                        start = null;
                    }
                    return;
                }
            }

            if (start == null)
                start = FindFirstPointInRange(m.X, m.Y, exchange.AreaSize/2);
            else
            {
                var tmp = FindFirstPointInRange(m.X, m.Y, exchange.AreaSize/2);
                if (tmp != null)
                {
                    Thread t = new Thread(delegate()
                    {
                        AStern astar = new AStern(exchange.Punkte, exchange.Verbindungen);
                        bool warned = false;
                        Parallel.Invoke(
                            () =>
                            {
                                if (!astar.RoutePossible(start.Bezeichnung, tmp.Bezeichnung))
                                {
                                    if (!warned)
                                    {
                                        warned = true;
                                        MessageBox.Show("Keine Route zu dem Punkt möglich!", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    }
                                }
                            },
                            () =>
                            {
                                var erg = astar.GetRoute(start.Bezeichnung, tmp.Bezeichnung);
                                if (erg == null)
                                {
                                    if (!warned)
                                    {
                                        warned = true;
                                        MessageBox.Show("Keine Route zu dem Punkt möglich!", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    }
                                }
                                else
                                {
                                    Pen p = new Pen(Color.Blue, 6.0f);
                                    picMaster.Invoke((MethodInvoker)delegate()
                                    {
                                        for (int i = 1; i < erg.Count; ++i)
                                        {
                                            Punkt a = erg[i - 1];
                                            Punkt b = erg[i];
                                            Draw(a, b, p);
                                        }
                                    });
                                }
                            }
                        );
                        start = null;
                    }) { IsBackground = true };
                    t.Start();
                }
            }
        }

        private void btnAllCon_Click(object sender, EventArgs e)
        {
            if (!loaded) return;
            foreach (var item in exchange.Verbindungen)
            {
                Draw(item);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            exchange = MapCreate.ShowSetupDialog();
            UpdateListBox();
            picMaster.Image = null;
            picMaster.Refresh();
        }
    }
}
