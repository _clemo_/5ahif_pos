﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using ConsoleAStern;

namespace FormAStern
{
    public partial class MapCreate : Form
    {
        public Exchange EX = null;
        
        private MapCreate()
        {
            InitializeComponent();
        }

        private void txtImgPath_Validating(object sender, CancelEventArgs e)
        {
            if (e.Cancel =  ((txtImgPath.Text != "") && !File.Exists(txtImgPath.Text)))
            {
                var res = MessageBox.Show("Die angegebe Datei existiert nicht!\nNach einer Datei suchen?", "Fehler", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                if (res == System.Windows.Forms.DialogResult.No)
                {
                    txtImgPath.Text = "";
                }
                else
                {
                    btnSearch_Click(sender, e);
                }
                
                var suff = txtImgPath.Text.Substring(txtImgPath.Text.LastIndexOf('.')).ToLower();
                if (!new List<string>() { ".jpg", ".bmp", ".png", ".tiff", ".gif" }.Contains(suff))
                {
                    e.Cancel = true;
                    MessageBox.Show("Die Datei muss eine Bild-Datei sein!", "Fehler!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            var fopendiag = new OpenFileDialog();
            fopendiag.Title = "Wähle Bild Datei";
            fopendiag.Filter = "Bilder|*.jpg;*.bmp;*.png;*.tiff;*.gif";
            if (fopendiag.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtImgPath.Text = fopendiag.FileName;
            }
        }

        private bool isNum(string text)
        {
            string charset = "0123456789";
            foreach (var item in text)
            {
                if (!charset.Contains(item))
                    return false;
            }
            return true;
        }

        private void textBox1_Validating(object sender, CancelEventArgs e)
        {
            if (e.Cancel = !isNum(txtRad.Text))
                MessageBox.Show("Es muss eine Zahl angegeben werden!", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if (e.Cancel = Int32.Parse(txtRad.Text) <= 5)
                    MessageBox.Show("Die Zahl darf nicht kleiner als 5px sein!", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static Exchange ShowSetupDialog()
        {
            var instance = new MapCreate();
            instance.ShowDialog();
            return instance.EX;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if(txtImgPath.Text.Trim() == ""){
                EX = null;
                var res = MessageBox.Show("Ohne Bilddatei kann das Programm nicht gestartet werden!\nSetup abbrechen?","Kein Bild ausgewählt!",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
                if(res == System.Windows.Forms.DialogResult.Yes)
                    this.Close();
            }
            EX = new Exchange() { Verbindungen = new List<Verbindung>(), Punkte = new List<Punkt>(), ImgPath = txtImgPath.Text, AreaSize = Int32.Parse(txtRad.Text) };
            this.Close();
        }
    }
}
