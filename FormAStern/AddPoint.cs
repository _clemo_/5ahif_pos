﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using ConsoleAStern;
using System.Windows.Forms;

namespace FormAStern
{
    public partial class AddPoint : Form
    {
        public Punkt RET = null;
        
        List<Punkt> knownPoints;
        public AddPoint(List<Punkt> knownPoints, int? x = null, int? y = null)
        {
            this.knownPoints = knownPoints;
            InitializeComponent();

            if (x != null)
            {
                txtX.Text = x.ToString();
            }

            if (y != null)
            {
                txtY.Text = y.ToString();
            }
        }

        private bool isNameInUse(string s)
        {
            if (s.Equals(" ")) return true;
            return knownPoints.Select(x => x.Bezeichnung).Contains(s);
        }

        private bool isCoordInUse(int x, int y)
        {
            return knownPoints.Where((p) => {return p.X == x && p.Y == y;}).Count() > 0;
        }

        private bool isNumber(string s)
        {
            string nums = "0123456789";
            foreach (var c in s)
            {
                if (!nums.Contains(c))
                    return false;
            }
            return true;
        }

        private void txt_Validating(object sender, CancelEventArgs e)
        {
            if (sender is TextBox)
            {
                e.Cancel = false;
                var s = sender as TextBox;
                if (!isNumber(s.Text))
                {
                    s.Text = "0";
                    e.Cancel = true;
                }
            }
        }

        private void txt_Coords(object sender, CancelEventArgs e)
        {
            txt_Validating(txtX, e);
            txt_Validating(txtY, e);

            int x = Int32.Parse(txtX.Text);
            int y = Int32.Parse(txtY.Text);

            if (isCoordInUse(x, y))
            {
                e.Cancel = true;
                MessageBox.Show("Die Koordinaten sind bereits in Verwendung!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }else
                e.Cancel = false;
        }

        private void txt_Bez(object sender, CancelEventArgs e)
        {
            if (txtDesc.Text.Equals(" ") || txtDesc.Text.Trim().Length == 0)
            {
                txtDesc.Focus();
                e.Cancel = true;
            }
            else if (isNameInUse(txtDesc.Text))
            {
                e.Cancel = true;
                txtDesc.Text = " ";
                MessageBox.Show("Die Bezeichnung ist bereits in Verwendung!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                e.Cancel = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CancelEventArgs c = new CancelEventArgs(false);
            txt_Bez(txtDesc, c);
            if (c.Cancel) return;
            txt_Coords(txtX, c);
            if (c.Cancel) return;

            RET = new Punkt(Int32.Parse(txtX.Text), Int32.Parse(txtY.Text), txtDesc.Text);
            this.Close();
        }
    }
}
