﻿using System;
using ConsoleAStern;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FormAStern
{
    public partial class AddCon : Form
    {
        List<Punkt> points;
        List<Verbindung> conns;

        public Verbindung CONN = null;

        public AddCon(List<Punkt> points, List<Verbindung> conns, Punkt A = null, Punkt B = null)
        {
            this.points = points;
            this.conns = conns;
            InitializeComponent();
            txtA.AutoCompleteCustomSource = new AutoCompleteStringCollection();
            txtA.AutoCompleteCustomSource.AddRange(points.Select(x=>x.Bezeichnung).ToArray());
            txtB.AutoCompleteCustomSource = new AutoCompleteStringCollection();
            txtB.AutoCompleteCustomSource.AddRange(points.Select(x => x.Bezeichnung).ToArray());

            txtA.Validated += txt_Validated;
            txtB.Validated += txt_Validated;

            txtDist.Validating += txtDist_Validating;

            if (A != null)
                txtA.Text = A.Bezeichnung;
            if (B != null)
                txtB.Text = B.Bezeichnung;
        }

        void txtDist_Validating(object sender, CancelEventArgs e)
        {
            double d;
            if (Double.TryParse(txtDist.Text, out d))
            {
                if (d < calcLen())
                {
                    e.Cancel = true;
                    txtDist.Text = d.ToString();
                    txtDist.Focus();
                }
                else
                    e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
                txtDist.Focus();
            }
        }

        Punkt getPoint(string desc)
        {
            var l = points.Where((p) => { return p.Bezeichnung.Equals(desc); });
            if (l.Count() == 0) return null;
            return l.First();
        }

        bool isConnectionKnown(string a, string b)
        {
            var l = conns.Where((x) => { return (x.NACH.Equals(a) && x.VON.Equals(b)) || (x.NACH.Equals(b) && x.VON.Equals(a)); });
            return l.Count() > 0;
        }

        double calcLen()
        {
            Punkt a = getPoint(txtA.Text), b = getPoint(txtB.Text);
            double _len = Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
            return _len;
        }

        void txt_Validated(object sender, EventArgs e)
        {
            if (txtA.Text.Trim().Length > 0 && txtB.Text.Trim().Length > 0)
            {
                txtDist.Text = calcLen().ToString();
            }
        }

        private void txtPoint_Validating(object sender, CancelEventArgs e)
        {
            e.Cancel = false;
            if (sender is TextBox)
            {
                var t = sender as TextBox;
                var l = points.Select(x => x.Bezeichnung).ToArray();
                e.Cancel = !l.Contains(t.Text);
                if (e.Cancel)
                    t.Focus();
                else
                {
                    if (txtA.Text.Trim().Length > 0 && txtB.Text.Trim().Length > 0)
                    {
                        if (txtA.Text.Equals(txtB.Text))
                        {
                            e.Cancel = true;
                            MessageBox.Show("Punkt A und B darf nicht gleich sein!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            t.Focus();
                        }
                        else if(isConnectionKnown(txtA.Text, txtB.Text))
                        {
                            e.Cancel = true;
                            MessageBox.Show("Die Verbindung exisitiert bereits!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            t.Focus();
                        }
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CONN = new Verbindung(txtA.Text, txtB.Text, Double.Parse(txtDist.Text));
            this.Close();
        }
    }
}
