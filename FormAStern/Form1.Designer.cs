﻿namespace FormAStern
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lbConnections = new System.Windows.Forms.ListBox();
            this.lbPoints = new System.Windows.Forms.ListBox();
            this.btnSaveData = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnAllCon = new System.Windows.Forms.Button();
            this.btnLoading = new System.Windows.Forms.Button();
            this.picMaster = new System.Windows.Forms.PictureBox();
            this.btnNew = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMaster)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(421, 314);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnNew);
            this.tabPage1.Controls.Add(this.lbConnections);
            this.tabPage1.Controls.Add(this.lbPoints);
            this.tabPage1.Controls.Add(this.btnSaveData);
            this.tabPage1.Controls.Add(this.btnLoad);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(413, 288);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Textual";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lbConnections
            // 
            this.lbConnections.FormattingEnabled = true;
            this.lbConnections.Location = new System.Drawing.Point(101, 34);
            this.lbConnections.Name = "lbConnections";
            this.lbConnections.Size = new System.Drawing.Size(306, 173);
            this.lbConnections.TabIndex = 3;
            // 
            // lbPoints
            // 
            this.lbPoints.FormattingEnabled = true;
            this.lbPoints.Location = new System.Drawing.Point(8, 34);
            this.lbPoints.Name = "lbPoints";
            this.lbPoints.Size = new System.Drawing.Size(87, 173);
            this.lbPoints.TabIndex = 2;
            // 
            // btnSaveData
            // 
            this.btnSaveData.Location = new System.Drawing.Point(84, 6);
            this.btnSaveData.Name = "btnSaveData";
            this.btnSaveData.Size = new System.Drawing.Size(75, 23);
            this.btnSaveData.TabIndex = 1;
            this.btnSaveData.Text = "Save Data";
            this.btnSaveData.UseVisualStyleBackColor = true;
            this.btnSaveData.Click += new System.EventHandler(this.btnSaveData_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(3, 6);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 0;
            this.btnLoad.Text = "Load Data";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnAllCon);
            this.tabPage2.Controls.Add(this.btnLoading);
            this.tabPage2.Controls.Add(this.picMaster);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(413, 288);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Map";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnAllCon
            // 
            this.btnAllCon.Location = new System.Drawing.Point(90, 7);
            this.btnAllCon.Name = "btnAllCon";
            this.btnAllCon.Size = new System.Drawing.Size(106, 23);
            this.btnAllCon.TabIndex = 2;
            this.btnAllCon.Text = "alle Verbindungen";
            this.btnAllCon.UseVisualStyleBackColor = true;
            this.btnAllCon.Click += new System.EventHandler(this.btnAllCon_Click);
            // 
            // btnLoading
            // 
            this.btnLoading.Location = new System.Drawing.Point(9, 7);
            this.btnLoading.Name = "btnLoading";
            this.btnLoading.Size = new System.Drawing.Size(75, 23);
            this.btnLoading.TabIndex = 1;
            this.btnLoading.Text = "load";
            this.btnLoading.UseVisualStyleBackColor = true;
            this.btnLoading.Click += new System.EventHandler(this.btnLoading_Click);
            // 
            // picMaster
            // 
            this.picMaster.Location = new System.Drawing.Point(8, 6);
            this.picMaster.Name = "picMaster";
            this.picMaster.Size = new System.Drawing.Size(399, 276);
            this.picMaster.TabIndex = 0;
            this.picMaster.TabStop = false;
            this.picMaster.Click += new System.EventHandler(this.picMaster_Click);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(165, 6);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 4;
            this.btnNew.Text = "neu";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 326);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "A*";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picMaster)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSaveData;
        private System.Windows.Forms.ListBox lbPoints;
        private System.Windows.Forms.ListBox lbConnections;
        private System.Windows.Forms.PictureBox picMaster;
        private System.Windows.Forms.Button btnAllCon;
        private System.Windows.Forms.Button btnLoading;
        private System.Windows.Forms.Button btnNew;
    }
}

